package com.ingooo.erp.erp.jxc;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.experimental.Accessors;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create by 丶TheEnd on 2020/1/14 0014.
 */
public class MenuTest {

    private static List<Menu> menuList;

    static {
        menuList = new ArrayList<>();
        menuList.add(new Menu(5, "二级菜单1", 1));
        menuList.add(new Menu(8, "二级菜单4", 4));
        menuList.add(new Menu(7, "二级菜单3", 3));
        menuList.add(new Menu(6, "二级菜单2", 2));

        menuList.add(new Menu(10, "二级菜单6", 2));
        menuList.add(new Menu(9, "二级菜单5", 1));
        menuList.add(new Menu(11, "二级菜单7", 3));
        menuList.add(new Menu(12, "二级菜单8", 4));

        menuList.add(new Menu(1, "顶级菜单1", 0));
        menuList.add(new Menu(2, "顶级菜单2", 0));
        menuList.add(new Menu(3, "顶级菜单3", 0));
        menuList.add(new Menu(4, "顶级菜单4", 0));

    }

    @Test
    void useFor() {
        Map<Integer, MenuDto> map = new HashMap<>(8);
        for (Menu menu : menuList) {
            MenuDto menuDto = getMenuDto(menu);
            if (menu.getParentId() == 0) {
                MenuDto dto = map.get(menu.getMenuId());
                if (dto == null) {
                    menuDto.setChild(new ArrayList<>());
                    map.put(menuDto.getMenuId(), menuDto);
                } else {
                    dto.setMenuName(menu.getMenuName());
                }
            } else {
                MenuDto dto = map.get(menu.getParentId());
                if (dto == null) {
                    dto = new MenuDto().setMenuId(menu.getParentId()).setChild(new ArrayList<>());
                    dto.getChild().add(menuDto);
                    map.put(menu.getParentId(), dto);
                } else {
                    map.get(menu.getParentId()).getChild().add(menuDto);
                }
            }
        }
        System.out.println(JSON.toJSONString(map.values()));
    }


    MenuDto getMenuDto(Menu menu) {
        return new MenuDto().setMenuId(menu.getMenuId()).setMenuName(menu.getMenuName());
    }

    @Data
    @Accessors(chain = true)
    static class Menu {

        private Integer menuId;

        private String menuName;

        private Integer parentId;


        public Menu() {
        }

        public Menu(Integer menuId, String menuName, Integer parentId) {
            this.menuId = menuId;
            this.menuName = menuName;
            this.parentId = parentId;
        }
    }

    @Data
    @Accessors(chain = true)
    static class MenuDto {
        private Integer menuId;

        private String menuName;

        private List<MenuDto> child;
    }

}
