package com.ingooo.erp.erp.jxc;

import cn.hutool.core.map.MapUtil;
import com.google.common.collect.Maps;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.HashSet;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

//@SpringBootTest
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Benchmark)
public class ErpJxcApplicationTests {

    static Map map2 = Maps.newHashMap();

    static {
        map2.put("a", "aa");
        map2.put("b", "bb");
        map2.put("c", "cc");
        map2.put("d", "dd");
        map2.put("e", "ee");
    }

    static long time;

    @BeforeAll
    public static void before(){
        time = System.currentTimeMillis();
    }

    @AfterAll
    public static void after(){
        System.out.println(System.currentTimeMillis() - time);
    }

    private void joinMapAsHuTool(){
        for (int j = 0; j < 1000000; j++) {
            MapUtil.join(map, "&", "=", null);
        }
    }


    @Test
    void testJoin(){
        joinMapAsHuTool();
    }


    @Test
    void contextLoads() {
        HashSet<String> strings = new HashSet<>();
        // 100W 重复次数
        // 105, 124, 107, 113, 106
        for (int i = 0; i < 2; i++) {
            String idCode = UUID.randomUUID().toString().substring(0, 8);
            System.out.println(idCode);
            strings.add(idCode);
        }
        System.out.println(strings.size());
    }


    private static LongAdder i = new LongAdder();
    private static Map map = Maps.newHashMap();

    static {
        map.put("a", "aa");
        map.put("b", "bb");
        map.put("c", "cc");
    }

    @TearDown
    public void tearDown(){
        System.out.println("i=" + i.longValue());
    }

    @Benchmark
    @Measurement(iterations = 1, batchSize = 10)
    @BenchmarkMode(Mode.SingleShotTime)
    public int sleepAWhile() throws Exception {
        i.increment();
        MapUtil.join(map, "&", "=", true);
        return 0;
    }

    @Test
    public void main() throws RunnerException {
        Options options = new OptionsBuilder().include(ErpJxcApplicationTests.class.getSimpleName())
                .forks(1).threads(10).build();
        new Runner(options).run();
    }
}
