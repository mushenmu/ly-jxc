package com.ingooo.erp.erp.jxc.model;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Create by 丶TheEnd on 2019/12/16 0016.
 * @author Administrator
 */
@Data
@Accessors(chain = true)
public class ResponseJson {

    private Integer code;

    private String msg;

    private Object data;

    public static ResponseJson checkError() {
        return checkError(null);
    }

    public static ResponseJson checkError(Object data){
        return new ResponseJson(-7, "校验失败", data);
    }

    public static ResponseJson httpMediaTypeNotSupported() {
        return httpMediaTypeNotSupported(null);
    }

    public static ResponseJson httpMediaTypeNotSupported(Object data) {
        return new ResponseJson(-6, "不支持的Http媒体类型", data);
    }

    public static ResponseJson requestMethodError(){
        return requestMethodError(null);
    }

    public static ResponseJson requestMethodError(Object data){
        return new ResponseJson(-5, "请求方法错误", data);
    }

    public static ResponseJson authError() {
        return authError(null);
    }

    public static ResponseJson authError(Object data) {
        return new ResponseJson(-4, "权限不足", data);
    }

    public static ResponseJson operationFil() {
        return new ResponseJson(-3, "操作失败", null);
    }

    public static ResponseJson operationFil(Object data) {
        return operationFil().setData(data);
    }

    public static ResponseJson parameterError(){
        return new ResponseJson(-2, "参数错误", null);
    }

    public static ResponseJson parameterError(Object data) {
        return parameterError().setData(data);
    }

    public static ResponseJson error(){
        return new ResponseJson(-1, "error", null);
    }

    public static ResponseJson error(Object data) {
        return error().setData(data);
    }

    public static ResponseJson success(){
        return new ResponseJson(1000, "success", null);
    }

    public static ResponseJson success(Object data) {
        return success().setData(data);
    }

    public ResponseJson() {
    }

    public ResponseJson(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
