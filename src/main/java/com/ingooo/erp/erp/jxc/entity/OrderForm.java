package com.ingooo.erp.erp.jxc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单 
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("orderForm")
public class OrderForm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 公司ID
     */
    @TableField("companyId")
    private Integer companyId;

    /**
     * 订单编号
     */
    @TableField("orderNumber")
    private String orderNumber;

    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 操作人
     */
    @TableField("userId")
    private Integer userId;

    /**
     * 订单状态：0：已创建，1,：创建审批， 2：转换为销售单审批
     */
    @TableField("orderStatus")
    private Integer orderStatus;


}
