package com.ingooo.erp.erp.jxc.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * Create by 丶TheEnd on 2019/12/17 0017.
 * @author Administrator
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class AdminDto {

    private Integer userId;

    @NotBlank(message = "用户名不能为空")
    private String userName;

    @NotBlank(message = "密码不能为空")
    private String password;

    @NotBlank(message = "手机号不能为空")
    private String phone;

    @NotBlank(message = "公司名称不能为空")
    private String companyName;

}
