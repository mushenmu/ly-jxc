package com.ingooo.erp.erp.jxc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ingooo.erp.erp.jxc.entity.SupplierType;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(rollbackFor = Exception.class)
public interface ISupplierTypeService extends IService<SupplierType> {
    List<SupplierType> getListSupplier(Integer companyId);
}

