package com.ingooo.erp.erp.jxc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 出库表 
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("stroagePop")
public class StroagePop implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 出库状态 0：已创建， 1：以审批，2：确认出库，-1：审批拒绝，-2：确认拒绝
     */
    @TableField("putStatus")
    private Integer putStatus;

    /**
     * 商品ID
     */
    @TableField("goodsId")
    private Integer goodsId;

    /**
     * 公司ID
     */
    @TableField("companyId")
    private Integer companyId;

    /**
     * 仓库ID
     */
    @TableField("warehouseId")
    private Integer warehouseId;

    /**
     * 数量
     */
    private Integer count;

    /**
     * 价格
     */
    private Long price;

    /**
     * 订单ID
     */
    @TableField("orderId")
    private Integer orderId;

    /**
     * 出库时间
     */
    @TableField("timePop")
    private Date timePop;


}
