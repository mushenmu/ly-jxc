package com.ingooo.erp.erp.jxc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 单位转换 
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("companyTransformation")
public class CompanyTransformation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 原单位
     */
    @TableField("sourceName")
    private String sourceName;

    /**
     * 原值
     */
    @TableField("sourceNumber")
    private Integer sourceNumber;

    /**
     * 目标单位
     */
    @TableField("targetName")
    private String targetName;

    /**
     * 目标值
     */
    @TableField("targetNumber")
    private Integer targetNumber;


}
