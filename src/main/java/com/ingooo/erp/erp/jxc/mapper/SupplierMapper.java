package com.ingooo.erp.erp.jxc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ingooo.erp.erp.jxc.entity.Supplier;
import com.ingooo.erp.erp.jxc.model.dto.supplier.SupplierDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 供应商  Mapper 接口
 * </p>
 * @author 申国祥
 * @date 2020年3月31日
 */
public interface SupplierMapper  extends BaseMapper<Supplier> {
    /**
     *通过公司Id获取
     * @param companyId
     * @return
     */
    @Select("SELECT supplier.*,suppliertype.`typeName` FROM supplier,suppliertype WHERE supplier.typeId=suppliertype.id AND supplier.companyId=${companyId}")
     List<SupplierDto> getSupplierStudent(@Param("companyId") Integer companyId);
}
