package com.ingooo.erp.erp.jxc.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;


/**
 * Create by 丶TheEnd on 2019/12/31 0031.
 * @author Administrator
 */
@Data
@Accessors(chain = true)
public class GoodsDto {

    private Integer id;

    private String name;

}
