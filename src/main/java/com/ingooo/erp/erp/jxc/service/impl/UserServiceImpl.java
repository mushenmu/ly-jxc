package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ingooo.erp.erp.jxc.entity.User;
import com.ingooo.erp.erp.jxc.entity.UserCompany;
import com.ingooo.erp.erp.jxc.mapper.UserMapper;
import com.ingooo.erp.erp.jxc.service.IUserCompanyService;
import com.ingooo.erp.erp.jxc.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 用户  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private IUserCompanyService userCompanyService;

    @Override
    public Page<User> getUserLikePhone(String phone, Page page) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.like("phone", phone);
        Page selectPage = this.baseMapper.selectPage(page, wrapper);
        return selectPage;
    }

    @Override
    public List<User> getUserListByCompanyId(Integer companyId, Page<User> page) {
        List<UserCompany> userCompanyList = userCompanyService.getUserCompanyByCompany(companyId, new Page<>(page.getCurrent(), page.getSize()));
        List<Integer> userIds = new LinkedList<>();
        for (UserCompany userCompany : userCompanyList) {
            userIds.add(userCompany.getUserId());
        }
        if (userIds == null || userIds.size() == 0) {
            return null;
        }
        List<User> users = this.baseMapper.selectBatchIds(userIds);
        return users;
    }

    @Override
    public boolean isExistsForPhone(String phone) {
        User user = getUserByPhone(phone);
        return user != null;
    }

    @Override
    public User getUserByPhone(String phone) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("phone", phone);
        User user = this.baseMapper.selectOne(wrapper);
        return user;
    }
}
