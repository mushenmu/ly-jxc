package com.ingooo.erp.erp.jxc.controller;


import com.ingooo.erp.erp.jxc.entity.Customer;
import com.ingooo.erp.erp.jxc.entity.CustomerType;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.service.ICustomerTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 客户类别  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@RestController
@RequestMapping("/customerType")
public class CustomerTypeController {
    @Autowired
    private ICustomerTypeService customerTypeService;
    /**
     * 通过公司ID获取客户集合
     * @param companyId
     * @return
     */
    @GetMapping("/login/customerType/company/{companyId}")
    public ResponseJson getCustomerList(@PathVariable Integer companyId) {
        List<CustomerType> customerList = customerTypeService.getListCustomer(companyId);
        return ResponseJson.success(customerList);
    }

    /**
     * 添加客户类别
     * @param customerType
     * @return
     */
    @PostMapping("/admin/customerType")
    public ResponseJson saveCustomer(@RequestBody CustomerType customerType) {
        customerTypeService.save(customerType);
        return ResponseJson.success();
    }
}
