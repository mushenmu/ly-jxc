package com.ingooo.erp.erp.jxc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 公司
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Company implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 公司ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 公司名称
     */
    @NotBlank(message = "公司名不能为空")
    private String name;

    /**
     * 公司负责人
     */
    @TableField("adminUserId")
    @NotNull(message = "公司负责人不能为空")
    private Integer adminUserId;

    /**
     * 公司唯一ID 用于搜索公司
     */
    @TableField("idCode")
    private String idCode;

    /**
     * 审批权限
     */
    @NotNull
    @TableField("approvalPower")
    private Integer approvalPower;

    /**
     * 上传文件权限
     */
    @NotNull
    @TableField("uploadPower")
    private Integer uploadPower;

    /**
     * 导入导出权限
     */
    @NotNull
    @TableField("incEncPower")
    private Integer incEncPower;

    /**
     * 财务权限
     */
    @NotNull
    @TableField("financePower")
    private Integer financePower;
}
