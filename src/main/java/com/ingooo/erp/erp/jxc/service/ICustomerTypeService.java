package com.ingooo.erp.erp.jxc.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ingooo.erp.erp.jxc.entity.Customer;
import com.ingooo.erp.erp.jxc.entity.CustomerType;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 客户类别  服务类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Transactional(rollbackFor = Exception.class)
public interface ICustomerTypeService extends IService<CustomerType> {
   List<CustomerType> getListCustomer( Integer companyId);
}
