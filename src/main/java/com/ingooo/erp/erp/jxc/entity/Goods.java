package com.ingooo.erp.erp.jxc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品 
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 公司ID
     */
    @TableField("companyId")
    private Integer companyId;

    /**
     * 商品名
     */
    @TableField("goodsName")
    private String goodsName;

    /**
     * 商品属性 1
     */
    @TableField("column1")
    private String column1;

    /**
     * 商品属性 2
     */
    @TableField("column2")
    private String column2;

    /**
     * 商品属性 3
     */
    @TableField("column3")
    private String column3;

    /**
     * 商品属性 4
     */
    @TableField("column4")
    private String column4;

    /**
     * 商品属性 5
     */
    @TableField("column5")
    private String column5;

    /**
     * 商品属性 6
     */
    @TableField("column6")
    private String column6;

    /**
     * 商品属性 7
     */
    @TableField("column7")
    private String column7;

    /**
     * 商品属性 8
     */
    @TableField("column8")
    private String column8;

    /**
     * 商品属性 9
     */
    @TableField("column9")
    private String column9;

    /**
     * 商品属性 10
     */
    @TableField("column10")
    private String column10;

    /**
     * 商品属性 11
     */
    @TableField("column11")
    private String column11;

    /**
     * 商品属性 12
     */
    @TableField("column12")
    private String column12;

    /**
     * 商品属性 13
     */
    @TableField("column13")
    private String column13;

    /**
     * 商品属性 14
     */
    @TableField("column14")
    private String column14;

    /**
     * 商品属性 15
     */
    @TableField("column15")
    private String column15;

    /**
     * 商品属性 16
     */
    @TableField("column16")
    private String column16;


    /**
     * 商品属性 17
     */
    @TableField("column17")
    private String column17;

    /**
     * 商品属性 18
     */
    @TableField("column18")
    private String column18;

    /**
     * 商品属性 19
     */
    @TableField("column19")
    private String column19;

    /**
     * 商品属性 20
     */
    @TableField("column20")
    private String column20;


}
