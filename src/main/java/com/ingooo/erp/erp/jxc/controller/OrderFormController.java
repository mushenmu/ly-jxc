package com.ingooo.erp.erp.jxc.controller;


import com.ingooo.erp.erp.jxc.entity.OrderForm;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.service.IOrderFormService;
import com.ingooo.erp.erp.jxc.util.PowerUtil;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.UUID;

/**
 * <p>
 * 订单  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@RestController
@RequestMapping("/order-form")
public class OrderFormController {

    @Autowired
    private IOrderFormService orderFormService;

    @Autowired
    private PowerUtil powerUtil;

    /**
     * 添加订单
     * @param orderForm
     * @return
     */
    public ResponseJson addOrderForm(@RequestBody OrderForm orderForm) {
        orderForm.setCreateTime(new Date());
        orderForm.setOrderNumber(UUID.randomUUID().toString().replaceAll("-", ""));
        if (powerUtil.isApproval(orderForm.getCompanyId())) {
            orderForm.setOrderStatus(0);
        } else {
            orderForm.setOrderStatus(2);
        }
        orderFormService.save(orderForm);
        return ResponseJson.success();
    }

    /**
     * 审批订单
     * @param orderId
     * @param status
     * @return
     */
    public ResponseJson approvalOrder(@PathVariable Integer orderId, @PathVariable Integer status) {
        if (status == -1 || status == 1) {
            OrderForm orderForm = orderFormService.getById(orderId);
            orderForm.setOrderStatus(status);
            orderFormService.updateById(orderForm);
            return ResponseJson.success();
        }
        return ResponseJson.parameterError("请选择正确的审批结果");
    }

    /**
     * 确认订单
     * @param orderId
     * @param status
     * @return
     */
    public ResponseJson enterOrder(@PathVariable Integer orderId, @PathVariable Integer status) {
        if (status == 2 || status == -2) {
            OrderForm orderForm = orderFormService.getById(orderId);
            orderForm.setOrderStatus(status);
            orderFormService.updateById(orderForm);
            return ResponseJson.success();
        }
        return ResponseJson.parameterError("请选择正确的确认结果");
    }


}
