package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ingooo.erp.erp.jxc.entity.Customer;
import com.ingooo.erp.erp.jxc.mapper.CustomerMapper;
import com.ingooo.erp.erp.jxc.model.dto.customer.CustommerDto;
import com.ingooo.erp.erp.jxc.service.ICustomerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 客户  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class CustomerServiceImpl extends ServiceImpl<CustomerMapper, Customer> implements ICustomerService {

    @Override
    public List<CustommerDto> getListByCompany(Integer companyId) {

        List<CustommerDto> customers = this.baseMapper.getCustomerStudent(companyId);
        return customers;
    }
}
