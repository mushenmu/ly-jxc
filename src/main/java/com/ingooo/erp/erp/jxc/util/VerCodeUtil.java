package com.ingooo.erp.erp.jxc.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Create by 丶TheEnd on 2019/12/18 0018.
 * @author Administrator
 */
public class VerCodeUtil {

    private static Map<String, Ver> map;

    static {
        map = new HashMap<>();
    }

    /**
     * 获取验证码
     * @param phone
     * @return
     */
    public static String getVer(String phone) {
        String code = String.valueOf(new Random().nextInt(8999) + 1000);
        map.put(phone, new Ver(code, phone, DateUtil.offsetMinute(new Date(),30)));
        return code;
    }

    /**
     * 验证验证码
     * @param phone
     * @param code
     * @return
     */
    public static boolean checkVer(String phone, String code) {
        if (StrUtil.hasEmpty(phone, code)) {
            return false;
        }
        Ver ver = map.get(phone);
        if (ver == null) {
            return false;
        }

        if (phone.equals(ver.phone) && code.equals(ver.code)) {
            if (ver.getTime() != null && System.currentTimeMillis() - ver.getTime().getTime() < 0) {
                return true;
            } else {
                map.put(phone, null);
            }
        }
        return false;
    }

    /**
     * 清除验证码
     * @param phone
     */
    public static void rmVer(String phone) {
        map.put(phone, null);
    }

    @Data
    @Accessors(chain = true)
    static class Ver{
        private String code;
        private String phone;
        private Date time;

        public Ver() {
        }

        public Ver(String code, String phone, Date time) {
            this.code = code;
            this.phone = phone;
            this.time = time;
        }
    }

}
