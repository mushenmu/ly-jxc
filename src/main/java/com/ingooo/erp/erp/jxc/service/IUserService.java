package com.ingooo.erp.erp.jxc.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ingooo.erp.erp.jxc.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 用户  服务类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Transactional(rollbackFor = Exception.class)
public interface IUserService extends IService<User> {

    /**
     * 通过手机号模糊查找用户
     * @param phone
     * @return
     */
    Page<User> getUserLikePhone(String phone, Page page);

    /**
     * 获取某个公司的全部员工
     * @param companyId
     * @return
     */
    List<User> getUserListByCompanyId(Integer companyId, Page<User> page);

    /**
     * 手机号是否已注册
     * @param phone
     * @return
     */
    boolean isExistsForPhone(String phone);

    /**
     * 通过手机号获取User
     * @param phone
     * @return
     */
    User getUserByPhone(String phone);

}
