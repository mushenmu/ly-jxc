package com.ingooo.erp.erp.jxc.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Create by 丶TheEnd on 2020/1/11 0011.
 */
@Data
@Accessors(chain = true)
public class UpdatePassword {

    private String phone;

    private String verCode;

    private String oldPassword;

    private String newPassword;

}
