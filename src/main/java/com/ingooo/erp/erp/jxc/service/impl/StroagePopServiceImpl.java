package com.ingooo.erp.erp.jxc.service.impl;

import com.ingooo.erp.erp.jxc.entity.StroagePop;
import com.ingooo.erp.erp.jxc.mapper.StroagePopMapper;
import com.ingooo.erp.erp.jxc.service.IStroagePopService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 出库表  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class StroagePopServiceImpl extends ServiceImpl<StroagePopMapper, StroagePop> implements IStroagePopService {

}
