package com.ingooo.erp.erp.jxc.controller;


import com.ingooo.erp.erp.jxc.entity.Goods;
import com.ingooo.erp.erp.jxc.entity.GoodsColumn;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.model.dto.GoodsDto;
import com.ingooo.erp.erp.jxc.service.IGoodsAttributeService;
import com.ingooo.erp.erp.jxc.service.IGoodsColumnService;
import com.ingooo.erp.erp.jxc.service.IGoodsService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 商品  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private IGoodsService goodsService;

    @Autowired
    private IGoodsAttributeService goodsAttributeService;

    @Autowired
    private IGoodsColumnService goodsColumnService;

    /**
     * 通过公司获取全部商品
     * @param companyId
     * @return
     */
    @GetMapping("/login/company/{companyId}")
    public ResponseJson getGoodsByCompany(@PathVariable Integer companyId) {
        List<Goods> goodsList = goodsService.getByCompany(companyId);
        return ResponseJson.success(goodsList);
    }

    /**
     * 添加商品
     * @param goods
     * @return
     */
    @PostMapping("/admin/goods")
    public ResponseJson addGoods(@RequestBody @Validated Goods goods) {
        goodsService.save(goods);
        return ResponseJson.success();
    }

    /**
     * 添加商品数据
     * @param goods
     * @return
     */
    @PostMapping("/admin/goods/data")
    public ResponseJson addGoodsData(@RequestBody Goods goods) {
        goodsService.save(goods);
        return ResponseJson.success();
    }

    /**
     * 修改商品数据
     * @param goods
     * @return
     */
    @PutMapping("/login/goods/data")
    public ResponseJson putGoodsData(@RequestBody Goods goods) {
        System.out.println(goods);
        boolean b = goodsService.updateById(goods);
        if (b) {
            return ResponseJson.success();
        } else {
            return ResponseJson.operationFil();
        }
    }

    /**
     * 获取字段名
     * @param companyId
     * @return
     */
    @GetMapping("/login/goods-column/company/{companyId}")
    public ResponseJson getGoodsColumnName(@PathVariable Integer companyId) {
        GoodsColumn goodsColumn = goodsColumnService.getGoodsColumnByGoods(companyId);
        return ResponseJson.success(goodsColumn);
    }

    /**
     * 修改字段名和可视装填
     * @param goodsColumn
     * @return
     */
    @PutMapping("/admin/goodsColumn")
    public ResponseJson updateGoodsColumn(@RequestBody GoodsColumn goodsColumn) {
        if (goodsColumn.getId() == null) {
            return ResponseJson.parameterError();
        }
        goodsColumnService.updateById(goodsColumn);
        return ResponseJson.success();
    }
}
