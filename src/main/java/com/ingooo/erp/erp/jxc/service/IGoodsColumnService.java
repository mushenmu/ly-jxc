package com.ingooo.erp.erp.jxc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ingooo.erp.erp.jxc.entity.GoodsColumn;

/**
 * Create by 丶TheEnd on 2020/1/10 0010.
 * @author Administrator
 */
public interface IGoodsColumnService extends IService<GoodsColumn> {

    /**
     * 通过商品ID获取字段名
     * @param goodsId
     * @return
     */
    GoodsColumn getGoodsColumnByGoods(Integer goodsId);

}
