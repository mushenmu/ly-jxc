package com.ingooo.erp.erp.jxc.service.impl;

import com.ingooo.erp.erp.jxc.entity.OrderForm;
import com.ingooo.erp.erp.jxc.mapper.OrderFormMapper;
import com.ingooo.erp.erp.jxc.service.IOrderFormService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class OrderFormServiceImpl extends ServiceImpl<OrderFormMapper, OrderForm> implements IOrderFormService {

}
