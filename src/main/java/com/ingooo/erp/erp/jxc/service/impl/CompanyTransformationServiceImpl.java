package com.ingooo.erp.erp.jxc.service.impl;

import com.ingooo.erp.erp.jxc.entity.CompanyTransformation;
import com.ingooo.erp.erp.jxc.mapper.CompanyTransformationMapper;
import com.ingooo.erp.erp.jxc.service.ICompanyTransformationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 单位转换  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class CompanyTransformationServiceImpl extends ServiceImpl<CompanyTransformationMapper, CompanyTransformation> implements ICompanyTransformationService {

}
