package com.ingooo.erp.erp.jxc.service;

import com.ingooo.erp.erp.jxc.entity.Goods;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 商品  服务类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Transactional(rollbackFor = Exception.class)
public interface IGoodsService extends IService<Goods> {

    /**
     * 通过公司获取全部商品
     * @param companyId
     * @return
     */
    List<Goods> getByCompany(Integer companyId);

}
