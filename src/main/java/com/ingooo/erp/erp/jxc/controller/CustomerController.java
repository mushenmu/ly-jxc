package com.ingooo.erp.erp.jxc.controller;


import com.ingooo.erp.erp.jxc.entity.Customer;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.model.dto.customer.CustommerDto;
import com.ingooo.erp.erp.jxc.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 客户  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private ICustomerService customerService;

    /**
     * 通过公司ID获取客户集合
     * @param companyId
     * @return
     */
    @GetMapping("/login/customer/company/{companyId}")
    public ResponseJson getCustomerList(@PathVariable Integer companyId) {
        List<CustommerDto> customerList = customerService.getListByCompany(companyId);
        return ResponseJson.success(customerList);
    }

    /**
     * 添加客户
     * @param customer
     * @return
     */
    @PostMapping("/admin/customer")
    public ResponseJson saveCustomer(@RequestBody Customer customer) {
        customerService.save(customer);
        return ResponseJson.success();
    }

    /**
     * 修改客户信息
     * @param customer
     * @return
     */
    @PutMapping("/admin/customer")
    public ResponseJson updateCustomer(@RequestParam Customer customer) {
        customerService.updateById(customer);
        return ResponseJson.success();
    }

    /**
     * 通过客户ID查询客户信息
     * @param customerId
     * @return
     */
    @GetMapping("/login/customer/{customerId}")
    public ResponseJson getById(@PathVariable Integer customerId) {
        Customer customer = customerService.getById(customerId);
        return ResponseJson.success(customer);
    }
}
