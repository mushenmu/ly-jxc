package com.ingooo.erp.erp.jxc.service;

import com.ingooo.erp.erp.jxc.entity.Customer;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ingooo.erp.erp.jxc.model.dto.customer.CustommerDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 客户  服务类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Transactional(rollbackFor = Exception.class)
public interface ICustomerService extends IService<Customer> {

    /**
     * 通过公司ID获取客户集合
     * @param companyId
     * @return
     */
    List<CustommerDto> getListByCompany(Integer companyId);

}
