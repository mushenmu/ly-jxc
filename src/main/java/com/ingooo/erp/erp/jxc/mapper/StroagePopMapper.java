package com.ingooo.erp.erp.jxc.mapper;

import com.ingooo.erp.erp.jxc.entity.StroagePop;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 出库表  Mapper 接口
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
public interface StroagePopMapper extends BaseMapper<StroagePop> {

}
