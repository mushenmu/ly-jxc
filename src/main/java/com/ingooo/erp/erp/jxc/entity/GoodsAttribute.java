package com.ingooo.erp.erp.jxc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品属性 
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("goodsAttribute")
public class GoodsAttribute implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品ID
     */
    @TableField("goodsId")
    private Integer goodsId;

    /**
     * 属性名
     */
    @TableField("attributeName")
    private String attributeName;

    /**
     * 商品属性值
     */
    @TableField("attributeValue")
    private String attributeValue;


}
