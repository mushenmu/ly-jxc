package com.ingooo.erp.erp.jxc.util;

import com.ingooo.erp.erp.jxc.entity.User;
import com.ingooo.erp.erp.jxc.entity.UserCompany;
import com.ingooo.erp.erp.jxc.service.IUserCompanyService;
import com.ingooo.erp.erp.jxc.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * Create by 丶TheEnd on 2019/12/16 0016.
 * @author Administrator
 */
@Validated
@Configuration
public class UserUtil {

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserCompanyService userCompanyService;

    @Autowired
    private HttpServletRequest request;

    /**
     * 当前登录用户是否为Root
     * @return
     */
    public boolean isRoot() {
        User user = getLoginUser();
        return user != null && user.getAuth() == 2;
    }

    /**
     * 当前登录用户是否为当前公司管理员
     * @return
     */
    public boolean isAdmin(@NotNull(message = "公司ID不能为空") Integer companyId){
        User user = getLoginUser();
        return user != null && user.getId() != null && userCompanyService.isAdmin(user.getId(), companyId);
    }

    /**
     * 获取当前登录用户
     * @return
     */
    public User getLoginUser() {
        return (User) request.getSession().getAttribute("user");
    }

    /**
     * 设置登录用户
     * @param user
     * @return
     */
    public User setLoginUser(User user) {
        request.getSession().setAttribute("user", user);
        ArrayList<UserCompany> userCompanies = new ArrayList<>();
        return user;
    }

    /**
     * 清除登录信息
     */
    public void removeLoginUser() {
        request.getSession().setAttribute("user", null);
    }

}
