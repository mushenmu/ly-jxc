package com.ingooo.erp.erp.jxc.model.dto.store;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Create by 丶TheEnd on 2020/1/15 0015.
 * @author Administrator
 */
@Data
@Accessors(chain = true)
public class StorePopDto {

    private Integer warehouseId;

    private Integer goodsId;

    private Integer companyId;

    private Integer orderId;

    private Integer number;

    private Long price;

}
