package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ingooo.erp.erp.jxc.entity.StoragePut;
import com.ingooo.erp.erp.jxc.mapper.StoragePutMapper;
import com.ingooo.erp.erp.jxc.service.IStoragePutService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 入库表  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class StoragePutServiceImpl extends ServiceImpl<StoragePutMapper, StoragePut> implements IStoragePutService {

}
