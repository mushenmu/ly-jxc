package com.ingooo.erp.erp.jxc.service;

import com.ingooo.erp.erp.jxc.entity.OrderForm;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 订单  服务类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Transactional(rollbackFor = Exception.class)
public interface IOrderFormService extends IService<OrderForm> {

}
