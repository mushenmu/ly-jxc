package com.ingooo.erp.erp.jxc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 账户 
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 标号
     */
    @TableField("accountName")
    @NotBlank(message = "商品名称不能为空")
    private String accountName;


    /**
     * 当前余额
     */
    @TableField("currentBalance")
    private Long currentBalance;

    /**
     * 期初余额
     */
    @TableField("earlyStageBalance")
    private Long earlyStageBalance;

    /**
     * 初始余额日期
     */
    @TableField("initBalanceDate")
    private LocalDate initBalanceDate;

    /**
     * 公司ID
     */
    @TableField("companyId")
    private Integer companyId;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 启用状态
     */
    private Integer status;


}
