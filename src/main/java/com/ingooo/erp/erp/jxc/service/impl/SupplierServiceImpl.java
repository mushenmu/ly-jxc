package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.ingooo.erp.erp.jxc.entity.Supplier;
import com.ingooo.erp.erp.jxc.mapper.SupplierMapper;
import com.ingooo.erp.erp.jxc.model.dto.supplier.SupplierDto;
import com.ingooo.erp.erp.jxc.service.ISupplierService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 供应商  服务实现类
 * </p>
 * @author 申国祥
 * @date 2020年3月31日
 */
@Service
public class SupplierServiceImpl extends ServiceImpl<SupplierMapper, Supplier> implements ISupplierService {
    /**
     *
     * @param companyId
     * @return
     */
    @Override
    public List<SupplierDto> getListByCompany(Integer companyId) {

        List<SupplierDto> suppliers = this.baseMapper.getSupplierStudent(companyId);
        return suppliers;

    }
}
