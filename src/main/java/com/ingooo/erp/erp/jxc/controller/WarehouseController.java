package com.ingooo.erp.erp.jxc.controller;

import com.ingooo.erp.erp.jxc.entity.User;
import com.ingooo.erp.erp.jxc.entity.Warehouse;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.service.IUserService;
import com.ingooo.erp.erp.jxc.service.IWarehouseService;
import com.ingooo.erp.erp.jxc.util.DtoUtil;
import com.ingooo.erp.erp.jxc.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 仓库表  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Validated
@RestController
@RequestMapping("/warehouse")
public class WarehouseController {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private UserUtil userUtil;

    @Autowired
    private DtoUtil dtoUtil;

    @Autowired
    private IWarehouseService warehouseService;

    @Autowired
    private IUserService userService;

    /**
     * 获取公司全部仓库
     * @param companyId
     * @return
     */
    @GetMapping("/login/company/{companyId}/warehouse")
    public ResponseJson getWarehouseByCompanyId(@PathVariable Integer companyId) {
        boolean admin = userUtil.isAdmin(companyId);
        if (admin) {
            List<Warehouse> warehouseByCompanyId = warehouseService.getWarehouseByCompanyId(companyId);
            return ResponseJson.success(dtoUtil.getWarehouseUserDtos(warehouseByCompanyId));
        }
        return ResponseJson.authError("权限不足");
    }

    /**
     * 添加仓库
     * @param warehouse
     * @return
     */
    @PostMapping("/admin/warehouse")
    public ResponseJson addWarehouse(@RequestBody @Validated Warehouse warehouse) {
        User user = userUtil.getLoginUser();

        if (warehouse.getAdminUserId() == null) {
            warehouse.setAdminUserId(user.getId());
        }

        warehouseService.save(warehouse);

        return ResponseJson.success();
    }

    /**
     * 删除仓库
     * @param id
     * @return
     */
    @DeleteMapping("/admin/warehouse/{id}")
    public ResponseJson deleteWarehouse(@PathVariable Integer id) {
        boolean b = warehouseService.removeById(id);
        if (b) {
            return ResponseJson.success();
        } else {
            return ResponseJson.operationFil("删除失败");
        }
    }

    /**
     * 修改仓库管理员
     * @param warehouseId
     * @param userId
     * @return
     */
    @PatchMapping("/admin/warehouse/{warehouseId}/admin/{userId}")
    public ResponseJson updateAdmin(@PathVariable Integer warehouseId,@PathVariable Integer userId){
        User user = userService.getById(userId);
        if (user == null) {
            return ResponseJson.parameterError("用户不存在");
        }
        Warehouse warehouse = warehouseService.getById(warehouseId);
        if (!warehouse.getCompanyId().equals(user.getCompanyId())) {
            return ResponseJson.authError("非法请求");
        }
        warehouse.setAdminUserId(userId);
        warehouseService.updateById(warehouse);
        return ResponseJson.success();
    }

    /**
     * 修改仓库信息
     * @param warehouse
     * @return
     */
    @PutMapping("/admin/warehouse")
    public ResponseJson updateWarehouse(@RequestBody @Validated Warehouse warehouse) {
        if (warehouse.getAdminUserId() == null) {
            return ResponseJson.parameterError("管理员不能为空");
        }
        Warehouse byId = warehouseService.getById(warehouse.getId());
        warehouseService.updateById(warehouse);
        return ResponseJson.success();
    }
}
