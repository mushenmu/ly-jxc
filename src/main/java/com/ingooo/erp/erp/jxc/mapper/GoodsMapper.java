package com.ingooo.erp.erp.jxc.mapper;

import com.ingooo.erp.erp.jxc.entity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品  Mapper 接口
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
public interface GoodsMapper extends BaseMapper<Goods> {

}
