package com.ingooo.erp.erp.jxc.service;

import com.ingooo.erp.erp.jxc.entity.StoragePut;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 入库表  服务类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Transactional(rollbackFor = Exception.class)
public interface IStoragePutService extends IService<StoragePut> {

}
