package com.ingooo.erp.erp.jxc.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ingooo.erp.erp.jxc.entity.Account;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 账户  服务类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Transactional(rollbackFor = Exception.class)
public interface IAccountService extends IService<Account> {

    /**
     * 获取公司的全部账户
     * @param companyId
     * @return
     */
    List<Account> getByCompany(Integer companyId);

    /**
     * 通过账户关键字模糊查找账户
     * @param accountName
     * @return
     */
   List<Account> getAccountLikeAccountName(@Param("accountName") String accountName);



}
