package com.ingooo.erp.erp.jxc.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Create by 丶TheEnd on 2020/1/2 0002.
 * @author Administrator
 */
@EnableTransactionManagement
@Configuration
public class MpConfig {

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

}
