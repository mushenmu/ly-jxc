package com.ingooo.erp.erp.jxc.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ingooo.erp.erp.jxc.entity.User;
import com.ingooo.erp.erp.jxc.entity.UserCompany;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-19
 */
public interface IUserCompanyService extends IService<UserCompany> {

    /**
     * 通过公司ID获取全部用户
     * @param companyId
     * @return
     */
    List<UserCompany> getUserCompanyByCompany(Integer companyId, Page<UserCompany> page);

    /**
     * 获取全部申请加入公司请求
     * @param companyId
     * @return
     */
    List<UserCompany> getJoinRequest(Integer companyId);

    /**
     * 是否可申请加入
     * @param userId
     * @param companyId
     * @return
     */
    boolean isMayApply(Integer userId, Integer companyId);

    /**
     * 是否已在此公司
     * @param userId
     * @param companyId
     * @return
     */
    boolean isInCompany(Integer userId, Integer companyId);

    /**
     * 通过userId和companyId获取对应的UserCompany对象
     * @param userId
     * @param companyId
     * @return
     */
    UserCompany getUserCompanyByUserAndCompany(Integer userId, Integer companyId);

    /**
     * 判断是否为管理员
     * @param userId
     * @param companyId
     * @return
     */
    boolean isAdmin(Integer userId, Integer companyId);

}
