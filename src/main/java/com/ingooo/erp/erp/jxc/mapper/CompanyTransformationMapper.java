package com.ingooo.erp.erp.jxc.mapper;

import com.ingooo.erp.erp.jxc.entity.CompanyTransformation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 单位转换  Mapper 接口
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
public interface CompanyTransformationMapper extends BaseMapper<CompanyTransformation> {

}
