package com.ingooo.erp.erp.jxc.controller;


import com.ingooo.erp.erp.jxc.entity.StockPile;
import com.ingooo.erp.erp.jxc.entity.StoragePut;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.model.dto.store.StorePutDto;
import com.ingooo.erp.erp.jxc.service.IStockPileService;
import com.ingooo.erp.erp.jxc.service.IStoragePutService;
import com.ingooo.erp.erp.jxc.util.PowerUtil;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 入库表  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@RestController
@RequestMapping("/storage-put")
public class StoragePutController {

    @Autowired
    private PowerUtil powerUtil;

    @Autowired
    private IStoragePutService storagePutService;

    @Autowired
    private IStockPileService stockPileService;

    /**
     * 商品入库
     * @param storePutDto
     * @return
     */
    @PostMapping("/login/goods/put")
    public ResponseJson addPut(@RequestBody @Validated StorePutDto storePutDto) {
        // 添加商品入库记录
        StoragePut storagePut = new StoragePut().setGoodsId(storePutDto.getGoodsId())
                .setCount(storePutDto.getNumber())
                .setPrice(storePutDto.getPrice())
                .setSurplus(null)
                .setOrderId(null)
                .setWarehouseId(storePutDto.getWarehouseId())
                .setTimePut(new Date());

        // 判断是否需要审批  如果不需要审批（取消审批功能，不查询公司的审批权限），直接删除本段代码即可
        if (powerUtil.isApproval(storePutDto.getCompanyId())) {
            // 设置状态为待审批
            storagePut.setPutStatus(0);
            storagePutService.save(storagePut);
            return ResponseJson.success();
        }

        // 设置状态为以发布
        storagePut.setPutStatus(2);
        storagePutService.save(storagePut);

        // 更新库存
        updateStockPile(storagePut);

        return ResponseJson.success();
    }

    /**
     * 审批入库
     * @param storePutId
     * @param status
     * @return
     */
    @PatchMapping("/login/store-put/approva/{storePutId}/status/{status}")
    public ResponseJson approvalPut(@PathVariable Integer storePutId, @PathVariable Integer status) {
        StoragePut storagePut = storagePutService.getById(storePutId);
        if (status == 1 || status == -1) {
            storagePut.setPutStatus(status);
            storagePutService.updateById(storagePut);
            // 取消二次审批 （如果不需要进行二次审批，在下面添加以下代码即可） enterPut(storePutId, status);
            return ResponseJson.success();
        }
        return ResponseJson.parameterError("请选择正确的审批结果");
    }

    /**
     * 确认入库
     * @param storePutId
     * @param status
     * @return
     */
    @PatchMapping("/login/store-put/enter/{storePutId}/status/{status}")
    public ResponseJson enterPut(@PathVariable Integer storePutId, @PathVariable Integer status) {
        StoragePut storagePut = storagePutService.getById(storePutId);

        if (status == -2) {
            storagePut.setPutStatus(-2);
        } else if (status == 2) {
            storagePut.setPutStatus(2);
            updateStockPile(storagePut);
        } else {
            return ResponseJson.parameterError("请选择正确的确认结果");
        }

        storagePutService.updateById(storagePut);
        return ResponseJson.success();

    }

    /**
     * 修改库存
     * @param storagePut
     */
    private void updateStockPile(StoragePut storagePut) {
        // 获取当前库存
        StockPile stockPile = stockPileService.getByWarehouseAndGoods(storagePut.getWarehouseId(), storagePut.getGoodsId());
        if (stockPile == null) {
            // 如果库存不存在，创建库存并保存
            stockPile = new StockPile().setCount(storagePut.getCount())
                    .setCreateTime(new Date())
                    .setGoodsId(storagePut.getGoodsId())
                    .setWarehouseId(storagePut.getWarehouseId())
                    .setPrice(storagePut.getPrice())
                    .setUpdateTime(new Date());
            stockPileService.save(stockPile);
        } else {
            // 修改库存
            stockPile.setCount(storagePut.getCount() + stockPile.getCount())
                    .setPrice(stockPile.getPrice() + storagePut.getPrice())
                    .setUpdateTime(new Date());
            stockPileService.updateById(stockPile);
        }
    }

}
