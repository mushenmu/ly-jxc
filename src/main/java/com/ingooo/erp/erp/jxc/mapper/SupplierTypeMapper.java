package com.ingooo.erp.erp.jxc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ingooo.erp.erp.jxc.entity.SupplierType;

public interface SupplierTypeMapper extends BaseMapper<SupplierType> {
}
