package com.ingooo.erp.erp.jxc.controller;


import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.service.IStockPileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 库存表  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@RestController
@RequestMapping("/stock-pile")
public class StockPileController {

    @Autowired
    private IStockPileService stockPileService;

    /**
     * 获取库存列表
     * @param warehouseId
     * @return
     */
    @GetMapping("/login/warehouse/{warehouseId}")
    public ResponseJson getStockPileByWarehouse(@PathVariable Integer warehouseId) {
        return ResponseJson.success(stockPileService.getByWarehouse(warehouseId));
    }

}
