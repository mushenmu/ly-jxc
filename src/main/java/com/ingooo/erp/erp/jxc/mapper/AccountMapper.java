package com.ingooo.erp.erp.jxc.mapper;

import com.ingooo.erp.erp.jxc.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账户  Mapper 接口
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
public interface AccountMapper extends BaseMapper<Account> {

}
