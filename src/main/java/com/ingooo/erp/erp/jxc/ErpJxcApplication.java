package com.ingooo.erp.erp.jxc;

import com.ingooo.erp.erp.jxc.model.ResponseJson;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Administrator
 */
@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.ingooo.erp.erp.jxc.mapper")
public class ErpJxcApplication {

    public static void main(String[] args) {
        SpringApplication.run(ErpJxcApplication.class, args);
    }

}
