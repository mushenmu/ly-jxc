package com.ingooo.erp.erp.jxc.controller;
import com.ingooo.erp.erp.jxc.entity.Supplier;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.model.dto.supplier.SupplierDto;
import com.ingooo.erp.erp.jxc.service.ISupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 供应商 前端控制器
 *
 * @author 申国祥
 * @date 2020年3月31日
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    private ISupplierService supplierService;
    /**
     * 通过公司ID获取供应商及和集合
     * @param companyId
     * @return
     */
    @GetMapping("/login/supplier/company/{companyId}")
    public ResponseJson getSupplierList(@PathVariable Integer companyId) {
        List<SupplierDto> supplierList = supplierService.getListByCompany(companyId);
        return ResponseJson.success(supplierList);
    }

    /**
     * 添加供应商
     * @param supplier
     * @return
     */
    @PostMapping("/admin/supplier")
    public ResponseJson saveSupplier(@RequestBody Supplier supplier) {
        supplierService.save(supplier);
        return ResponseJson.success();
    }

    /**
     * 修改供应商信息
     * @param supplier
     * @return
     */
    @PutMapping("/admin/supplier")
    public ResponseJson updateSupplier(@RequestParam Supplier supplier) {
        supplierService.updateById(supplier);
        return ResponseJson.success();
    }

    /**
     * 通过供应商ID查询供应商信息
     * @param supplierId
     * @return
     */
    @GetMapping("/login/supplier/{supplierId}")
    public ResponseJson getById(@PathVariable Integer supplierId) {
        Supplier supplier = supplierService.getById(supplierId);
        return ResponseJson.success(supplier);
    }
}
