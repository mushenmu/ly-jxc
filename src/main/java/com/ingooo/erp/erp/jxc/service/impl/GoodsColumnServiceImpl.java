package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ingooo.erp.erp.jxc.entity.GoodsColumn;
import com.ingooo.erp.erp.jxc.mapper.GoodsColumnMapper;
import com.ingooo.erp.erp.jxc.service.IGoodsColumnService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Create by 丶TheEnd on 2020/1/10 0010.
 * @author Administrator
 */
@Service
public class GoodsColumnServiceImpl extends ServiceImpl<GoodsColumnMapper, GoodsColumn> implements IGoodsColumnService {
    @Override
    public GoodsColumn getGoodsColumnByGoods(Integer goodsId) {
        QueryWrapper<GoodsColumn> wrapper = new QueryWrapper<>();
        wrapper.eq("companyId", goodsId);
        GoodsColumn goodsColumns = this.baseMapper.selectOne(wrapper);
        return goodsColumns;
    }
}
