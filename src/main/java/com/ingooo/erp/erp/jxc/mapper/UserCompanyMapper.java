package com.ingooo.erp.erp.jxc.mapper;

import com.ingooo.erp.erp.jxc.entity.UserCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-19
 */
public interface UserCompanyMapper extends BaseMapper<UserCompany> {

}
