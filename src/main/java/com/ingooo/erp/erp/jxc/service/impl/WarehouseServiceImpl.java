package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ingooo.erp.erp.jxc.entity.Warehouse;
import com.ingooo.erp.erp.jxc.mapper.WarehouseMapper;
import com.ingooo.erp.erp.jxc.service.IWarehouseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 仓库表  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class WarehouseServiceImpl extends ServiceImpl<WarehouseMapper, Warehouse> implements IWarehouseService {

    @Override
    public List<Warehouse> getWarehouseByCompanyId(Integer companyId) {
        QueryWrapper<Warehouse> wrapper = new QueryWrapper<>();
        wrapper.eq("companyId", companyId);
        List<Warehouse> warehouses = this.baseMapper.selectList(wrapper);
        return warehouses;
    }
}
