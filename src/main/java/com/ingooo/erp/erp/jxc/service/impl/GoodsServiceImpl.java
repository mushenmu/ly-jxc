package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ingooo.erp.erp.jxc.entity.Goods;
import com.ingooo.erp.erp.jxc.mapper.GoodsMapper;
import com.ingooo.erp.erp.jxc.service.IGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商品  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {

    @Override
    public List<Goods> getByCompany(Integer companyId) {
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        wrapper.eq("companyId", companyId);
        List<Goods> goods = this.baseMapper.selectList(wrapper);
        return goods;
    }
}
