package com.ingooo.erp.erp.jxc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Create by 丶TheEnd on 2020/1/10 0010.
 * @author Administrator
 */
@Data
@Accessors(chain = true)
@TableName("goodsColumn")
public class GoodsColumn {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("companyId")
    private Integer companyId;

    /**
     * 字段1名称
     */
    @TableField("column1Name")
    private String column1Name;

    /**
     * 字段1可视
     */
    @TableField("column1Show")
    private Integer column1Show;

    /**
     * 字段2名称
     */
    @TableField("column2Name")
    private String column2Name;

    /**
     * 字段2可视
     */
    @TableField("column2Show")
    private Integer column2Show;

    /**
     * 字段3名称
     */
    @TableField("column3Name")
    private String column3Name;

    /**
     * 字段3可视
     */
    @TableField("column3Show")
    private Integer column3Show;

    /**
     * 字段4名称
     */
    @TableField("column4Name")
    private String column4Name;

    /**
     * 字段4可视
     */
    @TableField("column4Show")
    private Integer column4Show;

    /**
     * 字段5名称
     */
    @TableField("column5Name")
    private String column5Name;

    /**
     * 字段5可视
     */
    @TableField("column5Show")
    private Integer column5Show;

    /**
     * 字段6名称
     */
    @TableField("column6Name")
    private String column6Name;

    /**
     * 字段6可视
     */
    @TableField("column6Show")
    private Integer column6Show;

    /**
     * 字段7名称
     */
    @TableField("column7Name")
    private String column7Name;

    /**
     * 字段7可视
     */
    @TableField("column7Show")
    private Integer column7Show;

    /**
     * 字段8名称
     */
    @TableField("column8Name")
    private String column8Name;

    /**
     * 字段8可视
     */
    @TableField("column8Show")
    private Integer column8Show;

    /**
     * 字段9名称
     */
    @TableField("column9Name")
    private String column9Name;

    /**
     * 字段9可视
     */
    @TableField("column9Show")
    private Integer column9Show;

    /**
     * 字段10名称
     */
    @TableField("column10Name")
    private String column10Name;

    /**
     * 字段10可视
     */
    @TableField("column10Show")
    private Integer column10Show;

    /**
     * 字段11名称
     */
    @TableField("column11Name")
    private String column11Name;

    /**
     * 字段11可视
     */
    @TableField("column11Show")
    private Integer column11Show;

    /**
     * 字段12名称
     */
    @TableField("column12Name")
    private String column12Name;

    /**
     * 字段12可视
     */
    @TableField("column12Show")
    private Integer column12Show;

    /**
     * 字段13名称
     */
    @TableField("column13Name")
    private String column13Name;

    /**
     * 字段13可视
     */
    @TableField("column13Show")
    private Integer column13Show;

    /**
     * 字段14名称
     */
    @TableField("column14Name")
    private String column14Name;

    /**
     * 字段14可视
     */
    @TableField("column14Show")
    private Integer column14Show;

    /**
     * 字段15名称
     */
    @TableField("column15Name")
    private String column15Name;

    /**
     * 字段1可视
     */
    @TableField("column15Show")
    private Integer column15Show;

    /**
     * 字段16名称
     */
    @TableField("column16Name")
    private String column16Name;

    /**
     * 字段16可视
     */
    @TableField("column16Show")
    private Integer column16Show;

    /**
     * 字段17名称
     */
    @TableField("column17Name")
    private String column17Name;

    /**
     * 字段17可视
     */
    @TableField("column17Show")
    private Integer column17Show;

    /**
     * 字段18名称
     */
    @TableField("column18Name")
    private String column18Name;

    /**
     * 字段18可视
     */
    @TableField("column18Show")
    private Integer column18Show;

    /**
     * 字段19名称
     */
    @TableField("column19Name")
    private String column19Name;

    /**
     * 字段19可视
     */
    @TableField("column19Show")
    private Integer column19Show;

    /**
     * 字段20名称
     */
    @TableField("column20Name")
    private String column20Name;

    /**
     * 字段20可视
     */
    @TableField("column20Show")
    private Integer column20Show;

    public static GoodsColumn GetDefault() {
        GoodsColumn goodsColumn = new GoodsColumn();
        goodsColumn
                .setColumn1Name("属性1").setColumn1Show(1)
                .setColumn2Name("属性2").setColumn2Show(1)
                .setColumn3Name("属性3").setColumn3Show(1)
                .setColumn4Name("属性4").setColumn4Show(1)
                .setColumn5Name("属性5").setColumn5Show(1)
                .setColumn6Name("属性6").setColumn6Show(1)
                .setColumn7Name("属性7").setColumn7Show(1)
                .setColumn8Name("属性8").setColumn8Show(1)
                .setColumn9Name("属性9").setColumn9Show(1)
                .setColumn10Name("属性10").setColumn10Show(1)
                .setColumn11Name("属性11").setColumn11Show(1)
                .setColumn12Name("属性12").setColumn12Show(1)
                .setColumn13Name("属性13").setColumn13Show(1)
                .setColumn14Name("属性14").setColumn14Show(1)
                .setColumn15Name("属性15").setColumn15Show(1)
                .setColumn16Name("属性16").setColumn16Show(1)
                .setColumn17Name("属性17").setColumn17Show(1)
                .setColumn18Name("属性18").setColumn18Show(1)
                .setColumn19Name("属性19").setColumn19Show(1)
                .setColumn20Name("属性20").setColumn20Show(1);
        return goodsColumn;
    }

}
