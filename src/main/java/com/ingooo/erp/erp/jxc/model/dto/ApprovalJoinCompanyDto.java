package com.ingooo.erp.erp.jxc.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * 审批加入公司申请
 * Create by 丶TheEnd on 2019/12/19 0019.
 * @author Administrator
 */
@Data
@Accessors(chain = true)
public class ApprovalJoinCompanyDto {

    /**
     * 用户ID
     */
    @NotNull(message = "用户ID不能为空")
    private Integer userId;

    /**
     * 公司ID
     */
    @NotNull(message = "公司ID不能为空")
    private Integer companyId;

    /**
     * 审批装填 0：未通过， 1：通过
     */
    @NotNull(message = "审批结果不能为空")
    private Integer status;

}
