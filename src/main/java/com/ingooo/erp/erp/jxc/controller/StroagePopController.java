package com.ingooo.erp.erp.jxc.controller;


import com.ingooo.erp.erp.jxc.entity.StockPile;
import com.ingooo.erp.erp.jxc.entity.StroagePop;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.model.dto.store.StorePopDto;
import com.ingooo.erp.erp.jxc.service.IStockPileService;
import com.ingooo.erp.erp.jxc.service.IStroagePopService;
import com.ingooo.erp.erp.jxc.util.PowerUtil;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * <p>
 * 出库表  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@RestController
@RequestMapping("/stroage-pop")
public class StroagePopController {

    @Autowired
    private PowerUtil powerUtil;

    @Autowired
    private IStroagePopService popService;

    @Autowired
    private IStockPileService stockPileService;

    /**
     * 添加出库记录
     * @param storePopDto
     * @return
     */
    @PostMapping("/login/goods/pop")
    public ResponseJson addPop(@RequestBody @Validated StorePopDto storePopDto) {

        // 添加商品出库记录
        StroagePop stroagePop = new StroagePop().setGoodsId(storePopDto.getGoodsId())
                .setCount(storePopDto.getNumber())
                .setOrderId(storePopDto.getOrderId())
                .setPrice(storePopDto.getPrice())
                .setTimePop(new Date())
                .setWarehouseId(storePopDto.getWarehouseId());

        /**
         * 判断是否可以出库
         */
        StockPile stockPile = mayPop(stroagePop);
        if (stockPile == null) {
            return ResponseJson.operationFil("库存不足");
        }

        // 判断是否需要审批  如果不需要审批（取消审批功能，不查询公司的审批权限），直接删除本段代码即可
        if (powerUtil.isApproval(storePopDto.getCompanyId())) {
            stroagePop.setPutStatus(0);
            popService.save(stroagePop);
            return ResponseJson.success();
        }

        // 设置状态为以发布
        stroagePop.setPutStatus(2);
        popService.save(stroagePop);

        // 更新库存
        updateStockPile(stroagePop, stockPile);

        return ResponseJson.success();
    }

    /**
     * 审批出库
     * @param storePopId
     * @param status
     * @return
     */
    @PatchMapping("/login/store-pop/approva/{storePopId]/status/{status}")
    public ResponseJson approvalPop(@PathVariable Integer storePopId, @PathVariable Integer status) {
        StroagePop stroagePop = popService.getById(storePopId);
        if (status == 1 || status == -1) {
            stroagePop.setPutStatus(status);
            popService.updateById(stroagePop);
            return ResponseJson.success();
        }
        return ResponseJson.parameterError("请选择正确的审批结果");
    }

    /**
     * 确认出库
     * @param storePopId
     * @param status
     * @return
     */
    @PatchMapping("/login/store-pop/enter/{storePopId}/status/{status}")
    public ResponseJson enterPop(@PathVariable Integer storePopId, @PathVariable Integer status) {
        StroagePop stroagePop = popService.getById(storePopId);
        if (status == -2) {
            stroagePop.setPutStatus(-2);
        } else if (status == 2) {
            stroagePop.setPutStatus(2);
            StockPile stockPile = mayPop(stroagePop);
            if (stockPile == null) {
                return ResponseJson.operationFil("库存不足");
            }
            updateStockPile(stroagePop, stockPile);
        } else {
            return ResponseJson.parameterError("请选择正确的确认结果");
        }
        popService.updateById(stroagePop);
        return ResponseJson.success();
    }

    /**
     * 能否出库（商品是否存在，库存是否存在，余量是否足够）
     * @param stroagePop
     * @return
     */
    private StockPile mayPop(StroagePop stroagePop) {
        // 获取当前库存
        StockPile stockPile = stockPileService.getByWarehouseAndGoods(stroagePop.getWarehouseId(), stroagePop.getGoodsId());

        if (stockPile == null) {
            return null;
        }

        return stockPile.getCount() - stroagePop.getCount() > 0 ? stockPile : null;
    }

    /**
     * 修改库存
     * @param stroagePop
     * @param stockPile
     * @return
     */
    private boolean updateStockPile(StroagePop stroagePop, StockPile stockPile) {

        // 修改库存
        stockPile.setCount(stockPile.getCount() - stroagePop.getCount())
                .setPrice(stockPile.getPrice() - stroagePop.getPrice())
                .setUpdateTime(new Date());
        stockPileService.updateById(stockPile);
        return true;
    }

}
