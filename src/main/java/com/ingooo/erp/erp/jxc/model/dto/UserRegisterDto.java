package com.ingooo.erp.erp.jxc.model.dto;

import com.ingooo.erp.erp.jxc.entity.User;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * Create by 丶TheEnd on 2019/12/19 0019.
 * @author Administrator
 */
@Data
@Accessors(chain = true)
public class UserRegisterDto {

    @NotBlank(message = "手机号不能为空")
    private String phone;

    @NotBlank(message = "用户名不能为空")
    private String userName;

    @NotBlank(message = "密码不能为空")
    private String password;

    @NotBlank(message = "验证码不能为空")
    private String verCode;

    public User toUser() {
        User user = new User();
        user.setPhone(this.phone)
                .setUserName(this.userName)
                .setPassword(this.password);
        return user;
    }

}
