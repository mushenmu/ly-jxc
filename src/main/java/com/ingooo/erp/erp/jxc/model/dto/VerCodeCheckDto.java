package com.ingooo.erp.erp.jxc.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * Create by 丶TheEnd on 2019/12/19 0019.
 * @author Administrator
 */
@Data
@Accessors(chain = true)
public class VerCodeCheckDto {

    @NotBlank(message = "手机号不能为空")
    private String phone;

    @NotBlank(message = "验证码不能为空")
    private String verCode;

}
