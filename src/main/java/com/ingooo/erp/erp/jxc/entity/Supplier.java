package com.ingooo.erp.erp.jxc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * 供应商
 *
 * @author 申国祥
 * @date 2020年3月31日
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Supplier {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 公司ID
     */
    @TableField("companyId")
    private Integer companyId;

    /**
     * 名称
     */
    private String name;

    /**
     * 类别ID
     */
    @TableField("typeId")
    private Integer typeId;

    /**
     * 联系人
     */
    private String contacts;

    /**
     * 联系电话
     */
    @TableField("contactNumber")
    private String contactNumber;

    /**
     * 微信、QQ
     */
    private String weixin;

    /**
     * 应付款初始余额
     */
    private Long iboap;

    /**
     * 应付款余额
     */
    private Long boap;

    /**
     * 余额日期
     */
    @TableField("balanceDate")
    private LocalDate balanceDate;

    /**
     * 纳税人识别码
     */
    @TableField("taxpayerNumber")
    private String taxpayerNumber;

    /**
     * 开户银行
     */
    @TableField("depositBank")
    private String depositBank;

    /**
     * 银行号码
     */
    @TableField("bankNumber")
    private String bankNumber;

    /**
     * 经营地址
     */
    @TableField("businessAddress")
    private String businessAddress;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 启用状态
     */
    private Integer status;


}
