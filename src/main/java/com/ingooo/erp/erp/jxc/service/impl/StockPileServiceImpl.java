package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ingooo.erp.erp.jxc.entity.StockPile;
import com.ingooo.erp.erp.jxc.mapper.StockPileMapper;
import com.ingooo.erp.erp.jxc.service.IStockPileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 库存表  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class StockPileServiceImpl extends ServiceImpl<StockPileMapper, StockPile> implements IStockPileService {

    @Override
    public List<StockPile> getByWarehouse(Integer warehouse) {
        QueryWrapper<StockPile> wrapper = new QueryWrapper<>();
        wrapper.eq("warehouseId", warehouse);
        List<StockPile> stockPiles = this.list(wrapper);
        return stockPiles;
    }

    @Override
    public StockPile getByWarehouseAndGoods(Integer warehouseId, Integer goodsId) {
        QueryWrapper<StockPile> wrapper = new QueryWrapper<>();
        wrapper.eq("warehouseId", warehouseId);
        wrapper.eq("goodsId", goodsId);
        StockPile stockPile = this.getOne(wrapper);
        return stockPile;
    }
}
