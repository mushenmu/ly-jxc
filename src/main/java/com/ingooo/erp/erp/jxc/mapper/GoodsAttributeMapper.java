package com.ingooo.erp.erp.jxc.mapper;

import com.ingooo.erp.erp.jxc.entity.GoodsAttribute;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品属性  Mapper 接口
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
public interface GoodsAttributeMapper extends BaseMapper<GoodsAttribute> {

}
