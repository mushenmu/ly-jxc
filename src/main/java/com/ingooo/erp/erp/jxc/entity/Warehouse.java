package com.ingooo.erp.erp.jxc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 仓库表 
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Warehouse implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 仓库ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 仓库名称
     */
    @NotBlank(message = "仓库名称不能为空")
    private String name;

    /**
     * 仓库地址
     */
    @NotBlank(message = "仓库地址不能空")
    private String address;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 公司ID
     */
    @TableField("companyId")
    private Integer companyId;

    /**
     * 仓管用户ID
     */
    @TableField("adminUserId")
    private Integer adminUserId;

    /**
     * 启用状态
     */
    private Integer status;


}
