package com.ingooo.erp.erp.jxc.controller;

import com.ingooo.erp.erp.jxc.entity.CustomerType;
import com.ingooo.erp.erp.jxc.entity.SupplierType;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.service.ISupplierTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/SupplierType")
public class SupplierTypeController {
    @Autowired
    private ISupplierTypeService supplierTypeService;

    /**
     * 通过公司ID获取供应商类别集合
     * @param companyId
     * @return
     */
    @GetMapping("/login/supplierType/company/{companyId}")
    public ResponseJson getSupplierTypeList(@PathVariable Integer companyId) {
        List<SupplierType> listSupplier = supplierTypeService.getListSupplier(companyId);
        return ResponseJson.success(listSupplier);
    }

     /**
     * 添加供应商类别
     * @param supplierType
     * @return
     */
    @PostMapping("/admin/supplierType")
    public ResponseJson saveSupplierType(@RequestBody SupplierType supplierType) {
        supplierTypeService.save(supplierType);
        return ResponseJson.success();
    }

}
