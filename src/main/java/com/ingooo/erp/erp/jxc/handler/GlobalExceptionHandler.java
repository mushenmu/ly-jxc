package com.ingooo.erp.erp.jxc.handler;

import com.ingooo.erp.erp.jxc.model.ResponseJson;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Create by 丶TheEnd on 2019/12/16 0016.
 * @author Administrator
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 自定义参数错误处理
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseJson handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        return ResponseJson.parameterError().setData(e.getBindingResult().getFieldError().getDefaultMessage());
    }

    /**
     * 请求方法出错处理
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseJson httpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        return ResponseJson.requestMethodError(e.getMessage());
    }

    /**
     * 参数不匹配错误处理
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseJson httpMessageNotReadableException(HttpMessageNotReadableException e) {
        return ResponseJson.parameterError(e.getMessage());
    }

    /**
     * 不支持的Http媒体类型请求错误处理
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseJson httpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException e){
        return ResponseJson.httpMediaTypeNotSupported(e.getMessage());
    }

    /**
     * 缺少必要的参数
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseJson missingServletRequestParameterException(MissingServletRequestParameterException e) {
        return ResponseJson.parameterError(e.getMessage());
    }
}
