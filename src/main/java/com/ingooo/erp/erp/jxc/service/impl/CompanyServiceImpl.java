package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ingooo.erp.erp.jxc.entity.Company;
import com.ingooo.erp.erp.jxc.entity.UserCompany;
import com.ingooo.erp.erp.jxc.mapper.CompanyMapper;
import com.ingooo.erp.erp.jxc.service.ICompanyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ingooo.erp.erp.jxc.service.IUserCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-17
 */
@Service
public class CompanyServiceImpl extends ServiceImpl<CompanyMapper, Company> implements ICompanyService {

    @Autowired
    private IUserCompanyService userCompanyService;

    @Override
    public List<Company> getCompanyListForUser(Integer userId) {
        QueryWrapper<UserCompany> userCompanyQueryWrapper = new QueryWrapper<>();
        userCompanyQueryWrapper.eq("userId", userId);
        List<UserCompany> userCompanyList = userCompanyService.list(userCompanyQueryWrapper);

        List<Company> companyList = new ArrayList<>();
        for (UserCompany userCompany : userCompanyList) {
            Company company = this.baseMapper.selectById(userCompany.getCompanyId());
            companyList.add(company);
        }
        return companyList;
    }

    @Override
    public Company getCompanyByIdCode(String idCode) {
        QueryWrapper<Company> wrapper = new QueryWrapper<>();
        wrapper.eq("idCode", idCode);
        Company company = this.baseMapper.selectOne(wrapper);
        return company;
    }
}
