package com.ingooo.erp.erp.jxc.service;

import com.ingooo.erp.erp.jxc.entity.Company;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-17
 */
public interface ICompanyService extends IService<Company> {

    /**
     * 获取用户所属公司列表
     * @param userId
     * @return
     */
    List<Company> getCompanyListForUser(Integer userId);

    /**
     * 通过 idCode 获取公司信息
     * @param idCode
     * @return
     */
    Company getCompanyByIdCode(String idCode);

}
