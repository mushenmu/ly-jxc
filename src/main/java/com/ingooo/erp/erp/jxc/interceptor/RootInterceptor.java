package com.ingooo.erp.erp.jxc.interceptor;

import com.alibaba.fastjson.JSON;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Create by 丶TheEnd on 2019/12/17 0017.
 * @author Administrator
 */
@Configuration
public class RootInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private UserUtil userUtil;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean root = userUtil.isRoot();
        if (root) {
            return true;
        } else {
            response.setStatus(200);
            response.setContentType("application/json; charset=utf-8");

            response.getWriter().write(JSON.toJSONString(ResponseJson.authError("当前不是Root用户")));
            return false;
        }
    }
}
