package com.ingooo.erp.erp.jxc.model.dto.company;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Create by 丶TheEnd on 2020/1/11 0011.
 * @author Administrator
 */
@Data
@Accessors(chain = true)
public class CompanyUserDto {


    /**
     * 公司ID
     */
    private Integer id;

    /**
     * 公司名称
     */
    private String name;

    /**
     * 公司负责人
     */
    private Integer adminUserId;

    private String adminUserName;

    /**
     * 公司唯一ID 用于搜索公司
     */
    private String idCode;

    /**
     * 审批权限
     */
    private Integer approvalPower;

    /**
     * 上传文件权限
     */
    private Integer uploadPower;

    /**
     * 导入导出权限
     */
    private Integer incEncPower;

    /**
     * 财务权限
     */
    private Integer financePower;

}
