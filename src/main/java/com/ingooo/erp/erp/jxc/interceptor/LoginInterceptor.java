package com.ingooo.erp.erp.jxc.interceptor;

import com.alibaba.fastjson.JSON;
import com.ingooo.erp.erp.jxc.entity.User;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 未登录拦截器
 * Create by 丶TheEnd on 2019/12/16 0016.
 * @author Administrator
 */
@Configuration
public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private UserUtil userUtil;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        User loginUser = userUtil.getLoginUser();
        if (loginUser != null) {
            return true;
        }

        response.setContentType("application/json;charset=utf-8");
        response.setStatus(200);

        ResponseJson authError = ResponseJson.authError("当前未登录");

        response.getWriter().write(JSON.toJSONString(authError));

        return false;
    }
}
