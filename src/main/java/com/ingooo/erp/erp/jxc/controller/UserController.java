package com.ingooo.erp.erp.jxc.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ingooo.erp.erp.jxc.entity.Company;
import com.ingooo.erp.erp.jxc.entity.User;
import com.ingooo.erp.erp.jxc.entity.UserCompany;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.model.dto.AdminDto;
import com.ingooo.erp.erp.jxc.model.dto.UpdatePassword;
import com.ingooo.erp.erp.jxc.model.dto.UserRegisterDto;
import com.ingooo.erp.erp.jxc.model.dto.VerCodeCheckDto;
import com.ingooo.erp.erp.jxc.service.ICompanyService;
import com.ingooo.erp.erp.jxc.service.IUserCompanyService;
import com.ingooo.erp.erp.jxc.service.IUserService;
import com.ingooo.erp.erp.jxc.util.UserUtil;
import com.ingooo.erp.erp.jxc.util.VerCodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Validated
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private UserUtil userUtil;

    @Autowired
    private ICompanyService companyService;

    @Autowired
    private IUserCompanyService userCompanyService;

    /**
     * 修改用户状态：禁用、启用
     * @param userId
     * @param status
     * @return
     */
    @PatchMapping("/root/user/{userId}/status/{status}")
    public ResponseJson upStatus(@PathVariable Integer userId, @PathVariable Integer status) {
        User user = userService.getById(userId);
        if (user == null) {
            return ResponseJson.operationFil("用户不存在");
        }
        if (status != 1 && status != 0) {
            return ResponseJson.parameterError("参数错误");
        }
        user.setStatus(status);
        userService.updateById(user);
        return ResponseJson.success();
    }

    /**
     * 通过手机号模糊查找用户 ROOT 用户
     * @param phone
     * @return
     */
    @GetMapping("/root/user/phone/{phone}")
    public ResponseJson getUserLikePhoneAsRoot(@PathVariable String phone, @RequestParam Integer pageNo, @RequestParam Integer pageSize) {
        Page<User> page = new Page<>(pageNo, pageSize);
        return ResponseJson.success(userService.getUserLikePhone(phone, page));
    }

    /**
     * 通过手机号查询 User
     * @param phone
     * @return
     */
    @GetMapping("/admin/company/{companyId}/user/phone/{phone}")
    public ResponseJson getUserByPhoneAsAdmin(@PathVariable Integer companyId, @PathVariable String phone) {
        boolean admin = userUtil.isAdmin(companyId);
        if (!admin) {
            return ResponseJson.authError("权限不足");
        }
        User userByPhone = userService.getUserByPhone(phone);
        if (userByPhone == null) {
            return ResponseJson.operationFil("用户不存在");
        }
        boolean inCompany = userCompanyService.isInCompany(userByPhone.getId(), companyId);
        if (inCompany) {
            return ResponseJson.success(userByPhone);
        } else {
            return ResponseJson.operationFil("本公司没有此用户");
        }
    }

    /**
     * 退出公司
     * @param companyId
     * @return
     */
    @DeleteMapping("/login/company/push/{companyId}")
    public ResponseJson pushCompany(@PathVariable Integer companyId) {
        User loginUser = userUtil.getLoginUser();
        UserCompany userCompany = userCompanyService.getUserCompanyByUserAndCompany(loginUser.getId(), companyId);
        if (userCompany == null) {
            return ResponseJson.operationFil("当前未加入该公司");
        }
        userCompany.setAuth(-1);
        userCompanyService.updateById(userCompany);
        return ResponseJson.success();
    }

    /**
     * 通过用户ID获取用户
     * 如果用户ID为空，返回当前已登录用户信息
     * 如果登录用户为 root ，返回用户ID对应user
     * 如果公司ID为空，返回参数错误
     * 如果是此公司管理员，返回用户ID对应user，否则返回权限不足
     * @param userId
     * @param companyId
     * @return
     */
    @GetMapping("/login/user/only")
    public ResponseJson getUserByUserIdAndCompanyId(@RequestParam(required = false) Integer userId, @RequestParam(required = false) Integer companyId) {
        if (userId == null) {
            return ResponseJson.success(userUtil.getLoginUser());
        }
        if (userUtil.isRoot()) {
            return ResponseJson.success(userService.getById(userId));
        }
        if (companyId == null) {
            return ResponseJson.parameterError("请选择公司");
        }
        if (userUtil.isAdmin(companyId)) {
            return ResponseJson.success(userService.getById(userId));
        }
        return ResponseJson.authError("权限不足");
    }

    /**
     * 获取用户列表
     * @return
     */
    @GetMapping("/login/user")
    public ResponseJson userListForRoot(@RequestParam(required = false) Integer companyId,
                                        @RequestParam(required = false) Integer pageSize,
                                        @RequestParam(required = false) Integer pageNo){
        Page<User> userPage = new Page<User>();
        if (pageSize != null && pageNo != null) {
            userPage = new Page<>(pageNo, pageSize);
        }
        // 如果公司ID为空并且是管理员的话返回全部用户列表
        if (companyId == null) {
            if (userUtil.isRoot()) {
                return ResponseJson.success(userService.page(userPage));
            }
        }
        // 判断当前是否为公司管理员
        if (userUtil.isAdmin(companyId)) {
            User loginUser = userUtil.getLoginUser();
            return ResponseJson.success(userService.getUserListByCompanyId(companyId, userPage));
        } else {
            return ResponseJson.authError("权限不足");
        }
    }

    /**
     * 申请加入公司
     * @param companyId
     * @return
     */
    @PostMapping("/login/company/{companyId}")
    public ResponseJson joinCompany(@PathVariable Integer companyId) {
        User loginUser = userUtil.getLoginUser();
        boolean mayApply = userCompanyService.isMayApply(loginUser.getId(), companyId);
        if (!mayApply) {
            return ResponseJson.operationFil("已申请加入或已在该公司");
        }
        UserCompany userCompany = userCompanyService.getUserCompanyByUserAndCompany(loginUser.getId(), loginUser.getCompanyId());

        if (userCompany == null) {
            userCompany = new UserCompany().setUserId(loginUser.getId()).setCompanyId(companyId).setAuth(0);
            userCompanyService.save(userCompany);
        } else {
            userCompany.setAuth(0);
            userCompanyService.updateById(userCompany);
        }
        return ResponseJson.success();
    }

    /**
     * 用户注册
     * @param userRegisterDto
     * @return
     */
    @PostMapping("/user")
    public ResponseJson register(@RequestBody @Validated UserRegisterDto userRegisterDto){
        boolean checkVer = VerCodeUtil.checkVer(userRegisterDto.getPhone(), userRegisterDto.getVerCode());
        if (!checkVer) {
            return ResponseJson.operationFil("验证码错误");
        }

        if (userService.isExistsForPhone(userRegisterDto.getPhone())) {
            return ResponseJson.operationFil("手机号已注册");
        }

        User user = userRegisterDto.toUser();

        user.setParent(0).setCompanyId(0).setAuth(0);

        userService.save(user);

        return ResponseJson.success();
    }

    /**
     * 校验验证码
     * @param verCodeCheckDto
     * @return
     */
    @PostMapping("/ver")
    public ResponseJson checkVer(@RequestBody @Validated VerCodeCheckDto verCodeCheckDto){

        if (VerCodeUtil.checkVer(verCodeCheckDto.getPhone(), verCodeCheckDto.getVerCode())) {
            return ResponseJson.success();
        } else {
            return ResponseJson.checkError("验证码错误");
        }

    }

    /**
     * 获取验证码
     * @param phone
     * @return
     */
    @GetMapping("/ver/{phone}")
    public ResponseJson getVer(@PathVariable String phone){
        String ver = VerCodeUtil.getVer(phone);
        System.out.println("手机号 ：" + phone + " ,  验证码：" + ver);
        return ResponseJson.success();
    }

    /**
     * 添加管理员
     * @param adminDto
     * @return
     */
    @Deprecated
    @PostMapping("/root/admin")
    public ResponseJson addAdmin(@RequestBody @Validated AdminDto adminDto) {
        boolean existsForPhone = userService.isExistsForPhone(adminDto.getPhone());

        if (existsForPhone) {
            return ResponseJson.operationFil("手机号已注册");
        }

        Company company = new Company().setName(adminDto.getCompanyName());
        companyService.save(company);
        User user = new User().setUserName(adminDto.getUserName())
                .setPassword(adminDto.getPassword())
                .setAuth(1)
                .setCompanyId(company.getId())
                .setParent(userUtil.getLoginUser().getId())
                .setPhone(adminDto.getPhone())
                .setStatus(1);
        userService.save(user);
        return ResponseJson.success();
    }

    /**
     * 添加普通用户
     * @param user
     * @return
     */
    @Deprecated
    @PostMapping("/admin/user")
    public ResponseJson addUser(@RequestBody @Validated User user) {

        boolean existsForPhone = userService.isExistsForPhone(user.getPhone());

        if (existsForPhone) {
            return ResponseJson.operationFil("手机号已注册");
        }
        User loginUser = userUtil.getLoginUser();
        user.setParent(loginUser.getId());
        user.setCompanyId(loginUser.getCompanyId());
        user.setAuth(0);
        userService.save(user);
        return ResponseJson.success();
    }

    /**
     * 登录
     * @param user
     * @return
     */
    @PostMapping("/toLogin")
    public ResponseJson login(@RequestBody @Validated User user) {
        User userByPhone = userService.getUserByPhone(user.getPhone());
        if (userByPhone == null) {
            return ResponseJson.operationFil("手机号不存在");
        }

        if (userByPhone.getStatus() != 1) {
            return ResponseJson.operationFil("手机号已禁用");
        }

        if (user.getPassword().equals(userByPhone.getPassword())) {
            userUtil.setLoginUser(userByPhone);
            return ResponseJson.success(userByPhone.getAuth());
        }

        return ResponseJson.operationFil("手机号或密码错误");
    }

    /**
     * 退出登录
     * @return
     */
    @GetMapping("/login/logout")
    public ResponseJson logout() {
        userUtil.setLoginUser(null);
        return ResponseJson.success();
    }

    /**
     * 找回密码
     * @param updatePassword
     * @return
     */
    @PutMapping("/pass/user/password")
    public ResponseJson findPassword(@RequestBody @NotNull(message = "参数错误") UpdatePassword updatePassword) {
        if (StrUtil.hasBlank(updatePassword.getPhone(), updatePassword.getVerCode(), updatePassword.getNewPassword())) {
            return ResponseJson.parameterError("参数错误");
        }
        boolean checkVer = VerCodeUtil.checkVer(updatePassword.getPhone(), updatePassword.getVerCode());
        if (!checkVer) {
            return ResponseJson.parameterError("验证码错误");
        }
        User user = userService.getById(userUtil.getLoginUser().getId());
        user.setPassword(updatePassword.getNewPassword());
        userService.updateById(user);
        userUtil.setLoginUser(user);
        return ResponseJson.success();
    }

    /**
     * 修改密码
     * @param updatePassword
     * @return
     */
    @PutMapping("/login/user/password")
    public ResponseJson updatePassword(@RequestBody @NotNull(message = "参数错误") UpdatePassword updatePassword) {
        User loginUser = userUtil.getLoginUser();
        if (StrUtil.hasBlank(updatePassword.getOldPassword(), updatePassword.getNewPassword())) {
            return ResponseJson.parameterError("密码不能为空");
        }
        if (!StrUtil.equals(updatePassword.getOldPassword(), loginUser.getPassword())) {
            return ResponseJson.operationFil("旧密码错误");
        }
        loginUser.setPassword(updatePassword.getNewPassword());
        userService.updateById(loginUser);
        userUtil.setLoginUser(null);
        return ResponseJson.success();
    }

    /**
     * 删除用户
     * @param userId
     * @return
     */
    @DeleteMapping("/login/admin/user/{userId}")
    public ResponseJson deleteUser(@PathVariable @NotNull(message = "参数异常") Integer userId) {
        userService.removeById(userId);
        return ResponseJson.success();
    }

    /**
     * 修改用户名
     * @param userName
     * @return
     */
    @PatchMapping("/login/user/userName")
    public ResponseJson updateUserName(@RequestBody @NotBlank(message = "用户名不能为空") String userName) {
        User user = userUtil.getLoginUser();
        user.setUserName(userName);
        userService.updateById(user);
        return ResponseJson.success();
    }

    /**
     * 获取当前登录用户信息
     * @return
     */
    @GetMapping("/login/user/mine")
    public ResponseJson getLoginUser() {
        return ResponseJson.success(userUtil.getLoginUser().setPassword(""));
    }

}
