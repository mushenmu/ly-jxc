package com.ingooo.erp.erp.jxc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品属性  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@RestController
@RequestMapping("//goods-attribute")
public class GoodsAttributeController {

}
