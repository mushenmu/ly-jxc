package com.ingooo.erp.erp.jxc.util;

import com.ingooo.erp.erp.jxc.entity.Company;
import com.ingooo.erp.erp.jxc.service.ICompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create by 丶TheEnd on 2020/1/14 0014.
 * @author Administrator
 */
@Component
public class PowerUtil {

    @Autowired
    private ICompanyService companyService;

    /**
     * 是否有审批权
     * @param companyId
     * @return
     */
    public boolean isApproval(Integer companyId) {
        Company company = companyService.getById(companyId);
        return (company == null || company.getApprovalPower() != 1);
    }

}
