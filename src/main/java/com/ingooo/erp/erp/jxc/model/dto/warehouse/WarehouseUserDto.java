package com.ingooo.erp.erp.jxc.model.dto.warehouse;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * Create by 丶TheEnd on 2020/1/11 0011.
 * @author Administrator
 */
@Data
@Accessors(chain = true)
public class WarehouseUserDto {

    private Integer id;

    private String name;

    private String address;

    private String remarks;

    private Integer companyId;

    private Integer adminUserId;

    private String adminUserName;

    private Integer status;

}
