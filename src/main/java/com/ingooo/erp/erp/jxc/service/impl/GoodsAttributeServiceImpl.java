package com.ingooo.erp.erp.jxc.service.impl;

import com.ingooo.erp.erp.jxc.entity.GoodsAttribute;
import com.ingooo.erp.erp.jxc.mapper.GoodsAttributeMapper;
import com.ingooo.erp.erp.jxc.service.IGoodsAttributeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品属性  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class GoodsAttributeServiceImpl extends ServiceImpl<GoodsAttributeMapper, GoodsAttribute> implements IGoodsAttributeService {

}
