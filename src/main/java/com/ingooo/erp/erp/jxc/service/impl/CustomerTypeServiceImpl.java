package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ingooo.erp.erp.jxc.entity.Customer;
import com.ingooo.erp.erp.jxc.entity.CustomerType;
import com.ingooo.erp.erp.jxc.mapper.CustomerTypeMapper;
import com.ingooo.erp.erp.jxc.service.ICustomerTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 客户类别  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class CustomerTypeServiceImpl extends ServiceImpl<CustomerTypeMapper, CustomerType> implements ICustomerTypeService {

    @Override
    public List<CustomerType> getListCustomer(Integer companyId) {
        QueryWrapper<CustomerType> wrapper = new QueryWrapper<>();
        wrapper.eq("companyId", companyId);
        List<CustomerType> customers = this.baseMapper.selectList(wrapper);
        return customers;
    }
}
