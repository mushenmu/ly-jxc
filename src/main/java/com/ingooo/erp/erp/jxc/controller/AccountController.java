package com.ingooo.erp.erp.jxc.controller;


import com.ingooo.erp.erp.jxc.entity.Account;
import com.ingooo.erp.erp.jxc.entity.User;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.service.IAccountService;
import com.ingooo.erp.erp.jxc.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 * 账户  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Validated
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private UserUtil userUtil;

    @Autowired
    private IAccountService accountService;

    /**
     * 添加账户
     * @param account
     * @return
     */
    @PostMapping("/admin/account")
    public ResponseJson addAccount(@RequestBody @Validated Account account) {
       /* User user = userUtil.getLoginUser();*/

       /* account.setCurrentBalance(account.getBalance())
                .setEarlyStageBalance(account.getBalance())
                .setInitBalanceDate(LocalDate.now())
                .setCompanyId(user.getCompanyId());*/
        accountService.save(account);

        return ResponseJson.success();
    }

    /**
     * 通过账户关键字获取账户详细信息
     * @param accountName
     * @return
     */
    @PostMapping ("/login/account/accountName")
    public ResponseJson getById(@RequestBody String accountName) {
        return ResponseJson.success(accountService.getAccountLikeAccountName(accountName));
    }

    /**
     * 获取某个公司的全部账户
     * @param companyId
     * @return
     */
    @GetMapping("/login/account/company/{companyId}")
    public ResponseJson getByCompanyId(@PathVariable Integer companyId) {
        List<Account> byCompany = accountService.getByCompany(companyId);
        return ResponseJson.success(byCompany);
    }

    /**
     * 修改账户信息
     * @param account
     * @return
     */
    @PutMapping("/admin/account")
    public ResponseJson updateCompany(@RequestBody Account account) {
        if (account.getId() != null) {
            accountService.updateById(account);
            return ResponseJson.success();
        } else {
            return ResponseJson.operationFil();
        }
    }

}
