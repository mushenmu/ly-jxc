package com.ingooo.erp.erp.jxc.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ingooo.erp.erp.jxc.entity.Company;
import com.ingooo.erp.erp.jxc.entity.GoodsColumn;
import com.ingooo.erp.erp.jxc.entity.User;
import com.ingooo.erp.erp.jxc.entity.UserCompany;
import com.ingooo.erp.erp.jxc.model.ResponseJson;
import com.ingooo.erp.erp.jxc.model.dto.ApprovalJoinCompanyDto;
import com.ingooo.erp.erp.jxc.model.dto.company.CompanyUserDto;
import com.ingooo.erp.erp.jxc.service.ICompanyService;
import com.ingooo.erp.erp.jxc.service.IGoodsColumnService;
import com.ingooo.erp.erp.jxc.service.IUserCompanyService;
import com.ingooo.erp.erp.jxc.service.IUserService;
import com.ingooo.erp.erp.jxc.util.DtoUtil;
import com.ingooo.erp.erp.jxc.util.UserUtil;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-17
 */
@Validated
@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private UserUtil userUtil;

    @Autowired
    private DtoUtil dtoUtil;

    @Autowired
    private ICompanyService companyService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserCompanyService userCompanyService;

    @Autowired
    private IGoodsColumnService columnService;

    /**
     * 通过 idCode 获取公司信息
     * @param idCode
     * @return
     */
    @GetMapping("/login/idCode/{idCode}")
    public ResponseJson getCompanyByIdCode(@PathVariable String idCode) {
        Company companyByIdCode = companyService.getCompanyByIdCode(idCode);
        return ResponseJson.success(companyByIdCode);
    }

    /**
     * 将员工设为离职状态
     * @param userId
     * @return
     */
    @DeleteMapping("/admin/company/{companyId}/user/push/{userId}")
    public ResponseJson pushUser(@PathVariable Integer companyId, @PathVariable Integer userId) {
        UserCompany adminUserCompany = userCompanyService.getUserCompanyByUserAndCompany(userUtil.getLoginUser().getId(), companyId);
        if (adminUserCompany.getAuth() != 2 && adminUserCompany.getAuth() != 3) {
            return ResponseJson.authError("权限不足");
        }
        UserCompany userCompany = userCompanyService.getUserCompanyByUserAndCompany(userId, companyId);
        if (userCompany == null) {
            return ResponseJson.operationFil("没有该员工");
        }
        userCompany.setAuth(-1);
        userCompanyService.updateById(userCompany);
        return ResponseJson.success();
    }

    /**
     * 通过公司ID获取公司信息
     * @param companyId
     * @return
     */
    @GetMapping("/pass/company/{companyId}")
    public ResponseJson getCompanyById(@PathVariable Integer companyId) {
        return ResponseJson.success(companyService.getById(companyId));
    }

    /**
     * 获取全部公司列表
     * @return
     */
    @GetMapping("/root/company")
    public ResponseJson companyListForRoot(@RequestParam(required = false) Integer pageNo,
                                           @RequestParam(required = false) Integer pageSize){
        if (pageNo != null && pageSize != null) {
            Page<Company> page = companyService.page(new Page<Company>(pageNo, pageSize));
            return ResponseJson.success(dtoUtil.getCompanyUserDto(page.getRecords()));
        }
        List<Company> list = companyService.list();
        return ResponseJson.success(dtoUtil.getCompanyUserDto(list));
    }

    /**
     * 获取自己所属公司列表
     * @return
     */
    @GetMapping("/login/company")
    public ResponseJson companyListForUser(){
        List<Company> companyList = companyService.getCompanyListForUser(userUtil.getLoginUser().getId());
        return ResponseJson.success(companyList);
    }

    /**
     * 获取全部待审批加入公司请求
     * @param companyId
     * @return
     */
    @GetMapping("/admin/company/{companyId}/join/request")
    public ResponseJson joinCompanyRequestList(@PathVariable Integer companyId) {
        User loginUser = userUtil.getLoginUser();
        if (!userCompanyService.isAdmin(loginUser.getId(), companyId)) {
            return ResponseJson.authError("权限不足");
        }

        List<UserCompany> joinRequest = userCompanyService.getJoinRequest(companyId);

        ArrayList<Integer> userIds = new ArrayList<>();

        for (UserCompany userCompany : joinRequest) {
            userIds.add(userCompany.getUserId());
        }

        List<User> users = userService.listByIds(userIds);

        for (User user : users) {
            user.setPassword("");
        }

        return ResponseJson.success(users);
    }

    /**
     * 审批加入公司申请
     * @param approvalJoinCompanyDto
     * @return
     */
    @PostMapping("/admin/company/join/request/approval")
    public ResponseJson approvalJoinCompany(@RequestBody @Validated ApprovalJoinCompanyDto approvalJoinCompanyDto) {
        if (approvalJoinCompanyDto.getStatus() != 1 && approvalJoinCompanyDto.getStatus() != 0) {
            return ResponseJson.parameterError("请正确选择审批结果");
        }

        User loginUser = userUtil.getLoginUser();

//        boolean admin = userCompanyService.isAdmin(loginUser.getId(), approvalJoinCompanyDto.getCompanyId());
//
//        if (!admin) {
//            return ResponseJson.authError("权限不足");
//        }

        UserCompany userCompany = userCompanyService.getUserCompanyByUserAndCompany(approvalJoinCompanyDto.getUserId(), approvalJoinCompanyDto.getCompanyId());
        if (userCompany.getAuth() == 0) {
            if (approvalJoinCompanyDto.getStatus() == 0) {
                userCompany.setAuth(-2);
            } else if (approvalJoinCompanyDto.getStatus() == 1) {
                userCompany.setAuth(1);
            } else {
                return ResponseJson.parameterError("审批结果错误");
            }
            userCompanyService.updateById(userCompany);
            return ResponseJson.success();
        } else {
            return ResponseJson.operationFil("用户为申请加入");
        }

    }

    /**
     * 添加公司
     * @param company
     * @return
     */
    @PostMapping("/root/company")
    public ResponseJson saveCompany(@RequestBody @Validated Company company) {
        User user = userService.getById(company.getAdminUserId());
        if (user == null) {
            return ResponseJson.parameterError("公司负责人不存在");
        }
        user.setAuth(1);
        userService.updateById(user);

        String idCode;
        while (true) {
            idCode = UUID.randomUUID().toString().substring(0, 8);
            Company companyByIdCode = companyService.getCompanyByIdCode(idCode);
            if (companyByIdCode == null) {
                break;
            }
        }
        companyService.save(company.setIdCode(idCode));

        UserCompany userCompany = new UserCompany().setAuth(3).setCompanyId(company.getId()).setUserId(company.getAdminUserId());
        userCompanyService.save(userCompany);

        GoodsColumn goodsColumn = GoodsColumn.GetDefault().setCompanyId(company.getId());
        columnService.save(goodsColumn);
        return ResponseJson.success();
    }

    /**
     * 修改公司信息
     * @param company
     * @return
     */
    @PutMapping("/root/company")
    public ResponseJson updateCompany(@RequestBody @Validated Company company){
        if (company.getId() == null) {
            return ResponseJson.parameterError("请选择公司");
        }
        companyService.updateById(company);
        return ResponseJson.success();
    }

    /**
     * 管理员修改公司名
     * @param company
     * @return
     */
    @PatchMapping("/admin/companyName")
    public ResponseJson updateCompanyName(@RequestBody @NotNull(message = "参数错误") Company company) {
        if (company.getId() == null) {
            return ResponseJson.parameterError("请选择公司");
        }
        if (company.getName() == null) {
            return ResponseJson.parameterError("公司名不能为空");
        }
        boolean admin = userCompanyService.isAdmin(userUtil.getLoginUser().getId(), company.getId());
        if (!admin) {
            return ResponseJson.authError("权限不足");
        }
        Company byId = companyService.getById(company.getId());
        byId.setName(company.getName());
        companyService.updateById(byId);
        return ResponseJson.success();
    }
}
