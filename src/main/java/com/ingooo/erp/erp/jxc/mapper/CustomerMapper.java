package com.ingooo.erp.erp.jxc.mapper;

import com.ingooo.erp.erp.jxc.entity.Customer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ingooo.erp.erp.jxc.model.dto.customer.CustommerDto;
import com.ingooo.erp.erp.jxc.model.dto.supplier.SupplierDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 客户  Mapper 接口
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
public interface CustomerMapper extends BaseMapper<Customer> {
        /**
     *通过公司Id获取
     * @param companyId
     * @return
     */
    @Select("SELECT customer.*,customertype.`typeName` FROM customer,customertype WHERE customer.typeId=customertype.id AND customer.companyId=${companyId}")
    List<CustommerDto> getCustomerStudent(@Param("companyId") Integer companyId);

}
