package com.ingooo.erp.erp.jxc.service;

import com.ingooo.erp.erp.jxc.entity.StockPile;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 库存表  服务类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Transactional(rollbackFor = Exception.class)
public interface IStockPileService extends IService<StockPile> {

    /**
     * 获取仓库全部库存
     * @param warehouse
     * @return
     */
    List<StockPile> getByWarehouse(Integer warehouse);

    /**
     * 通过仓库和商品获取库存
     * @param warehouseId
     * @param goodsId
     * @return
     */
    StockPile getByWarehouseAndGoods(Integer warehouseId, Integer goodsId);

}
