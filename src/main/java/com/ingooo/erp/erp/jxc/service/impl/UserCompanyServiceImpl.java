package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ingooo.erp.erp.jxc.entity.UserCompany;
import com.ingooo.erp.erp.jxc.mapper.UserCompanyMapper;
import com.ingooo.erp.erp.jxc.service.IUserCompanyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-19
 */
@Service
public class UserCompanyServiceImpl extends ServiceImpl<UserCompanyMapper, UserCompany> implements IUserCompanyService {

    @Override
    public List<UserCompany> getUserCompanyByCompany(Integer companyId, Page<UserCompany> userCompanyPage) {
        QueryWrapper<UserCompany> wrapper = new QueryWrapper<>();
        wrapper.eq("companyId", companyId);
        Page<UserCompany> userCompanyPage1 = this.baseMapper.selectPage(userCompanyPage, wrapper);
        return userCompanyPage1.getRecords();
    }

    @Override
    public List<UserCompany> getJoinRequest(Integer companyId) {
        QueryWrapper<UserCompany> wrapper = new QueryWrapper<>();
        wrapper.eq("companyId", companyId);
        wrapper.eq("auth", 0);
        List<UserCompany> userCompanies = this.baseMapper.selectList(wrapper);
        return userCompanies;
    }

    @Override
    public boolean isMayApply(Integer userId, Integer companyId) {
        UserCompany userCompany = getUserCompanyByUserAndCompany(userId, companyId);
        System.out.println(userId + ", " + companyId);
        System.out.println(userCompany);
        return userCompany == null || userCompany.getAuth() == -1 || userCompany.getAuth() == -2;
    }

    @Override
    public boolean isInCompany(Integer userId, Integer companyId) {
        UserCompany userCompany = getUserCompanyByUserAndCompany(userId, companyId);
        return userCompany != null;
    }

    @Override
    public UserCompany getUserCompanyByUserAndCompany(Integer userId, Integer companyId) {
        QueryWrapper<UserCompany> wrapper = new QueryWrapper<>();
        wrapper.eq("userId", userId);
        wrapper.eq("companyId", companyId);
        UserCompany userCompany = this.baseMapper.selectOne(wrapper);
        return userCompany;
    }

    @Override
    public boolean isAdmin(Integer userId, Integer companyId) {
        UserCompany userCompany = getUserCompanyByUserAndCompany(userId, companyId);
        return userCompany != null && (userCompany.getAuth() == 2 || userCompany.getAuth() == 3);
    }
}
