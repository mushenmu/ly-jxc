package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ingooo.erp.erp.jxc.entity.Account;
import com.ingooo.erp.erp.jxc.entity.User;
import com.ingooo.erp.erp.jxc.mapper.AccountMapper;
import com.ingooo.erp.erp.jxc.service.IAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 账户  服务实现类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {

    @Override
    public List<Account> getByCompany(Integer companyId) {
        QueryWrapper<Account> wrapper = new QueryWrapper<>();
        wrapper.eq("companyId", companyId);
        List<Account> accounts = this.baseMapper.selectList(wrapper);
        return accounts;
    }

    /**
     * 通过账户关键字获取账户详细信息
     * @param accountName
     * @return
     */
    @Override
    public List<Account> getAccountLikeAccountName( @Param("accountName") String accountName) {
        QueryWrapper<Account> wrapper = new QueryWrapper<>();
        wrapper.like("accountName",accountName);
        List<Account> accounts = this.baseMapper.selectList(wrapper);
        return accounts;
    }
}
