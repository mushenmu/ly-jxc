package com.ingooo.erp.erp.jxc.model.dto.supplier;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import java.time.LocalDate;

@Data

public class SupplierDto {
      private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 公司ID
     */
    @TableField("companyId")
    private Integer companyId;

    /**
     * 名称
     */
    private String name;

    /**
     * 类别ID
     */
    @TableField("typeId")
    private Integer typeId;

    /**
     * 类别名称
     */
    private String typeName;

    /**
     * 联系人
     */
    private String contacts;

    /**
     * 联系电话
     */
    @TableField("contactNumber")
    private String contactNumber;

    /**
     * 微信、QQ
     */
    private String weixin;

    /**
     * 应付款初始余额
     */
    private Long iboap;

    /**
     * 应付款余额
     */
    private Long boap;

    /**
     * 余额日期
     */
    @TableField("balanceDate")
    private LocalDate balanceDate;

    /**
     * 纳税人识别码
     */
    @TableField("taxpayerNumber")
    private String taxpayerNumber;

    /**
     * 开户银行
     */
    @TableField("depositBank")
    private String depositBank;

    /**
     * 银行号码
     */
    @TableField("bankNumber")
    private String bankNumber;

    /**
     * 经营地址
     */
    @TableField("businessAddress")
    private String businessAddress;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 启用状态
     */
    private Integer status;


}
