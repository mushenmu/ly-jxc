package com.ingooo.erp.erp.jxc.config;

import com.ingooo.erp.erp.jxc.interceptor.AdminInterceptor;
import com.ingooo.erp.erp.jxc.interceptor.LoginInterceptor;
import com.ingooo.erp.erp.jxc.interceptor.RootInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.ArrayList;

/**
 * Create by 丶TheEnd on 2019/12/17 0017.
 * @author Administrator
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Autowired
    private LoginInterceptor loginInterceptor;

    @Autowired
    private AdminInterceptor adminInterceptor;

    @Autowired
    private RootInterceptor rootInterceptor;

    /**
     * 配置拦截器
     * @param registry
     */
    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        ArrayList<String> userUrlList = new ArrayList<>();
        userUrlList.add("/*/login/**");

        ArrayList<String> adminUrlList = new ArrayList<>();
        adminUrlList.add("/*/admin/**");

        ArrayList<String> rootUrlList = new ArrayList<>();
        rootUrlList.add("/*/root/**");

        registry.addInterceptor(loginInterceptor).addPathPatterns(userUrlList);
//        registry.addInterceptor(adminInterceptor).addPathPatterns(adminUrlList);
        registry.addInterceptor(rootInterceptor).addPathPatterns(rootUrlList);
    }

    /**
     * 配置跨域访问
     * @param registry
     */
    @Override
    protected void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "DELETE", "PUT","PATCH")
                .maxAge(3600);
    }

    @Bean
    public CorsFilter corsFilter(){
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedOrigin("*");
        configuration.addAllowedMethod("*");
        configuration.addAllowedHeader("*");
        configuration.setAllowCredentials(true);
        configuration.setMaxAge(3600L);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return new CorsFilter(source);
    }
}
