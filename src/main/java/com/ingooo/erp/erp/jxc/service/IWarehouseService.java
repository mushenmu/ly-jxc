package com.ingooo.erp.erp.jxc.service;

import com.ingooo.erp.erp.jxc.entity.Warehouse;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 仓库表  服务类
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
@Transactional(rollbackFor = Exception.class)
public interface IWarehouseService extends IService<Warehouse> {

    /**
     * 通过公司ID获取仓库
     * @param companyId
     * @return
     */
    List<Warehouse> getWarehouseByCompanyId(Integer companyId);

}
