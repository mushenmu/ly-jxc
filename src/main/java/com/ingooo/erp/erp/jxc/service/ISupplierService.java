package com.ingooo.erp.erp.jxc.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ingooo.erp.erp.jxc.entity.Supplier;
import com.ingooo.erp.erp.jxc.model.dto.supplier.SupplierDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
  * <p>
 * 供应商  服务类
 * </p>
 *
 * @author 申国祥
 * @date 2020年3月31日
 */
@Transactional(rollbackFor = Exception.class)
public interface ISupplierService extends IService<Supplier> {
 /**
     * 通过公司ID获取供应商集合
     * @param companyId
     * @return
     */
    List<SupplierDto> getListByCompany(Integer companyId);

}
