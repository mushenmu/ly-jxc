package com.ingooo.erp.erp.jxc.util;

import com.alibaba.fastjson.JSON;
import com.ingooo.erp.erp.jxc.entity.Company;
import com.ingooo.erp.erp.jxc.entity.Warehouse;
import com.ingooo.erp.erp.jxc.model.dto.company.CompanyUserDto;
import com.ingooo.erp.erp.jxc.model.dto.warehouse.WarehouseUserDto;
import com.ingooo.erp.erp.jxc.service.IUserService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * DTO工具类，用于将Model转换为DTO
 * Create by 丶TheEnd on 2020/1/11 0011.
 * @author Administrator
 */
@Component
public class DtoUtil {

    @Autowired
    private IUserService userService;

    /**
     * 获取仓库用户DTO集合
     * @param warehouseList
     * @return
     */
    public List<WarehouseUserDto> getWarehouseUserDtos(List<Warehouse> warehouseList) {
        ArrayList<WarehouseUserDto> list = new ArrayList<>();
        for (Warehouse warehouse : warehouseList) {
            list.add(getWarehouseUserDto(warehouse));
        }
        return list;
    }

    /**
     * 获取仓库用户DTO
     * @param warehouse
     * @return
     */
    public WarehouseUserDto getWarehouseUserDto(Warehouse warehouse) {
        WarehouseUserDto dto = convert(warehouse, new WarehouseUserDto());
        dto.setAdminUserName(userService.getById(warehouse.getAdminUserId()).getUserName());
        return dto;
    }

    public List<CompanyUserDto> getCompanyUserDto(List<Company> companies) {
        LinkedList<CompanyUserDto> list = new LinkedList<>();
        for (Company company : companies) {
            list.add(getCompanyUserDto(company));
        }
        return list;
    }

    /**
     * 获取公司DTO
     * @param company
     * @return
     */
    public CompanyUserDto getCompanyUserDto(Company company){
        return convert(company, new CompanyUserDto()).setAdminUserName(userService.getById(company.getAdminUserId()).getUserName());
    }

    /**
     * 把对象转换为对应的DTO
     * @param t
     * @param k
     * @param <T>
     * @param <K>
     * @return
     */
    public static <T, K> K convert(T t, K k) {
        k = (K) JSON.parseObject(JSON.toJSONString(t), k.getClass());
        return k;
    }

}
