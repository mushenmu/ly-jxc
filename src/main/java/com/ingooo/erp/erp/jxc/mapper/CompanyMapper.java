package com.ingooo.erp.erp.jxc.mapper;

import com.ingooo.erp.erp.jxc.entity.Company;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-17
 */
public interface CompanyMapper extends BaseMapper<Company> {

}
