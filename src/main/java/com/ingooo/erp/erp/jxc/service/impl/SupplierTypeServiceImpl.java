package com.ingooo.erp.erp.jxc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ingooo.erp.erp.jxc.entity.CustomerType;
import com.ingooo.erp.erp.jxc.entity.SupplierType;
import com.ingooo.erp.erp.jxc.mapper.SupplierTypeMapper;
import com.ingooo.erp.erp.jxc.service.ISupplierTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 客户类别  服务实现类
 * @author 申国祥
 * @date 2020年3月31日
 */
@Service
public class SupplierTypeServiceImpl extends ServiceImpl<SupplierTypeMapper, SupplierType> implements ISupplierTypeService {
    @Override
    public List<SupplierType> getListSupplier(Integer companyId) {
         QueryWrapper<SupplierType> wrapper = new QueryWrapper<>();
        wrapper.eq("companyId", companyId);
        List<SupplierType> supplierTypes = this.baseMapper.selectList(wrapper);
        return supplierTypes;
    }
}
