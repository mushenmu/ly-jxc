package com.ingooo.erp.erp.jxc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ingooo.erp.erp.jxc.entity.GoodsColumn;

/**
 * Create by 丶TheEnd on 2020/1/10 0010.
 * @author Administrator
 */
public interface GoodsColumnMapper extends BaseMapper<GoodsColumn> {
}
