package com.ingooo.erp.erp.jxc.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ingooo.erp.erp.jxc.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ingooo.erp.erp.jxc.entity.UserCompany;

/**
 * <p>
 * 用户  Mapper 接口
 * </p>
 *
 * @author 丶TheEnd
 * @since 2019-12-16
 */
public interface UserMapper extends BaseMapper<User> {

    void selectList(QueryWrapper<UserCompany> wrapper);
}
